### Spring Cloud
Spring Cloud是一系列框架的有序集合。它利用Spring Boot的开发便利性巧妙地简化了分布式系统基础设施的开发，如服务发现注册、配置中心、消息总线、负载均衡、断路器、数据监控等，都可以用Spring Boot的开发风格做到一键启动和部署。Spring Cloud并没有重复制造轮子，它只是将目前各家公司开发的比较成熟、经得起实际考验的服务框架组合起来，通过Spring Boot风格进行再封装屏蔽掉了复杂的配置和实现原理，最终给开发者留出了一套简单易懂、易部署和易维护的分布式系统开发工具包。
![](../_media/SpringCloud_demo.png)
#### Spring Cloud核心子项目



#### SpringCloud服务组合

SpringCloud生态强调微服务，微服务也就意味着将各个功能独立的业务抽象出来，做成一个单独的服务供外部调用。但每个人对服务究竟要有多“微”的理解差异很大，导致微服务的粒度很难掌控，划分规则也不统一。这导致的一个问题就是在实现一个业务场景的过程中，无法避免的需要对微服务进行整合。本文提出了一种对微服务进行组合的方案，来解决上述的问题。

#### Spring Cloud Alibaba
Spring Cloud Alibaba 致力于提供微服务开发的一站式解决方案。此项目包含开发分布式应用微服务的必需组件，方便开发者通过 Spring Cloud 编程模型轻松使用这些组件来开发分布式应用服务。
[``]()
![](../_media/alibabaSpringCloud.png)
![](../_media/alibabaSpringCloud_1.png)

##### 服务注册 & 配置管理
Spring Cloud Alibaba 基于 Nacos 提供 spring-cloud-alibaba-starter-nacos-discovery & spring-cloud-alibaba-starter-nacos-config 实现了服务注册 & 配置管理功能。

##### 流控降级
Spring Cloud Alibaba 基于 Sentinel 提供 spring-cloud-alibaba-starter-sentinel 对 Spring 体系内基本所有的客户端，网关进行了适配，包括了 WebServlet, WebFlux, RestTemplate, OpenFeign, Netflix Zuul, Spring Cloud Gateway。

##### 服务调用
Spring Cloud Alibaba Dubbo 进行 RPC 调用。

##### 分布式消息 & 消息总线
RocketMQ

##### 分布式事务
[Seata](https://seata.io/zh-cn/) 是 阿里巴巴 开源的 分布式事务中间件，以 高效 并且对业务 0 侵入 的方式，解决 微服务 场景下面临的分布式事务问题。