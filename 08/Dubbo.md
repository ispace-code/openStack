### Dubbo
[Seata](https://seata.io/zh-cn/) 是一款阿里巴巴开源的分布式事务解决方案，致力于在微服务架构下提供高性能和简单易用的分布式事务服务。
[`https://github.com/seata/seata`](https://github.com/seata/seata)