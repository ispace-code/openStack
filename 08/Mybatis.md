### MyBatis
![](../_media/mybatis-logo.png) 
[MyBatis](https://mybatis.org/mybatis-3/zh/index.html) 的前身就是 iBatis 。是一个Java 数据持久层框架(ORM)框架。
iBATIS一词来源于“internet”和“abatis”的组合，是一个基于Java的持久层框架。iBATIS提供的持久层框架包括SQL Maps和Data Access Objects（DAO）

[`https://github.com/mybatis/mybatis-3`](https://github.com/mybatis/mybatis-3)

![](../_media/mybatis.png)