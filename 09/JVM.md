### Maven 简介
### Maven 安装
### JVM 调优

#### Java 内存泄露
* Performance(性能):通常与过多的对象创建和删除，垃圾收集的长时间延迟，过多的操作系统页面交换等相关联。
* Resource constraints(资源约束)：当可用内存很少或内存过于分散而无法分配大对象时 - 这可能是本机的，或者更常见的是与Java堆相关。
* Java heap leaks(java堆泄漏):经典的内存泄漏，Java对象在不释放的情况下不断创建。这通常是由潜在对象引用引起的。
* Native memory leaks(本机内存泄漏):与Java堆之外的任何不断增长的内存利用率相关联，例如由JNI代码，驱动程序甚至JVM分配。
