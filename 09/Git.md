### Git 基本操作（常用命令）

| 命令 | 含义 |
| ---------------------- | --------------------------------------------- |
| git init              | Git 创建仓库 |
| git clone          |  git clone 从现有 Git 仓库中拷贝项目 |
| git add         | git add 命令可将该文件添加到缓存 |
| git status      | git status 以查看在你上次提交之后是否有修改。 |
| git diff      | 执行 git diff 来查看执行 git status 的结果的详细信息。 |
| git commit       | 使用 git add 命令将想要快照的内容写入缓存区， 而执行 git commit 将缓存区内容添加到仓库中 |
| git reset HEAD | git reset HEAD 命令用于取消已缓存的内容。 |
| git rm | Git 中移除某个文件，就必须要从已跟踪文件清单中移除，然后提交。 |
| git mv | git mv 命令用于移动或重命名一个文件、目录、软连接。 |
| git branch (branchname) | 创建分支 |
| git branch -d (branchname) | 删除分支 |
| git branch              | 列出分支 |
| git checkout (branchname) | 切换分支 |
| git rebase | 分支变基 |
| git merge | 合并分支 |
| git log | 列出历史提交记录|
| git tag -a (tagname) | 使用 git tag 给它打上标签 |
| git remote add [shortname] [url] | 添加一个新的远程仓库 |

### Git 服务器搭建


