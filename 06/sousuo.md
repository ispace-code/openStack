### Apache Lucene
![](http://lucene.apache.org/images/lucene_logo_green_300.png)
Lucene 是 apache 软件基金会的一个子项目，由 Doug Cutting 开发，是一个开放源代码的全文检索引擎工具包，但它不是一个完整的全文检索引擎，而是一个全文检索引擎的库，提供了完整的查询引擎和索引引擎，部分文本分析引擎（英文与德文两种西方语言）。Lucene 的目的是为软件开发人员提供一个简单易用的工具包，以方便的在目标系统中实现全文检索的功能，或者是以此为基础建立起完整的全文检索引擎。
### Solr
Solr是一个高性能，采用Java开发，基于Lucene的全文搜索服务器。同时对其进行了扩展，提供了比Lucene更为丰富的查询语言，同时实现了可配置、可扩展并对查询性能进行了优化，并且提供了一个完善的功能管理界面，是一款非常优秀的全文搜索引擎。

### Elasticsearch
![]()
ElasticSearch是一个基于Lucene的搜索服务器。它提供了一个分布式多用户能力的全文搜索引擎，基于RESTful web接口。Elasticsearch是用Java语言开发的，并作为Apache许可条款下的开放源码发布，是一种流行的企业级搜索引擎。ElasticSearch用于云计算中，能够达到实时搜索，稳定，可靠，快速，安装使用方便。

### Senseidb
SenseiDB是一个NoSQL数据库，它专注于高更新率以及复杂半结构化搜索查询。熟悉Lucene和Solor的用户会发现，SenseiDB背后有许多似曾相识的概念。SenseiDB部署在多节点集群中，其中每个节点可以包括N块数据片。Apache Zookeeper用于管理节点，它能够保持现有配置，并可以将任意改动（如拓扑修改）传输到整个节点群中。SenseiDB集群还需要一种模式用于定义将要使用的数据模型。
![](../_media/senseidb.png)

#### 功能
* 全文检索
* 实时更新
* faceted search
* key-value查询
* 在高并发更新与查询性能高
* 支持与Hadoop集成

SenseiDB系统主要由以下几部分组成
* `Sensei Node` 实际处理索引以及检索请求的地方，保存索引数据，内部分为几个模块：
* `Zoie System` 通过zoie保存索引数据，提供实时索引服务。在一个服务器上可以同时保存多个分区的数据，每个分区对应一个ZoieSystem对象
* `Data Provider` 负责从Data Gateway获取数据并传给ZoieSystem建索引，在分发数据的时候会根据分区信息进行分发
* `Broker` 负责接受用户查询，并根据均衡策略以及负载均衡分发到不同Sensei Node上进行查询，并对分布在不同分区上的数据进行合并，然后格式化输出给用户
* `ZooKeeper` 用于维护服务器节点集群的列表等信息，便于各个服务器节点直接协作。其信息基本上不需要人为的修改，只需要配置好每个节点自己的配置（如节点编号、存储的分区列表等），节点启动后会自动修改ZooKeeper中的数据。因此Sensei的集群管理相当简单。
* `Data Gateway` 负责提供sensei数据源的模块，目前支持以下几种数据源：
   1. jms  通过topic订阅的方式让所有sensei node获取到数据，用于在线更新的模式，但是没法提供安全恢复的功能
   2. 文件 从指定文件获取索引的数据，多用于新建索引时导入原始数据
   3. jdbc 实际上是以数据库实现的持久化消息队列，需要有一个数据库表，里面带有一个类似时间戳的属性，sensei node定时来获取上一个最后获取的版本之后的数据来建索引
   4. kafka linkedin自己实现的一个消息队列，还没有调研过
实际上，在sensei的实现中，broker和Sensei Node是在同一个进程中实现的，并没有物理上的分离。