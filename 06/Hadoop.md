### 概念

就是一个大数据解决方案。它提供了一套分布式系统基础架构。 核心内容包含 hdfs 和 mapreduce。hadoop2.0 以后引入 yarn. hdfs 是提供数据存储的，mapreduce 是方便数据计算的。
1. hdfs 又对应 namenode 和 datanode. namenode 负责保存元数据的基本信息，datanode 直接存放数据本身；
2. mapreduce 对应 jobtracker 和 tasktracker. jobtracker 负责分发任务，tasktracker 负责执行具体任务；
3. 对应到 master/slave 架构，namenode 和 jobtracker 就应该对应到 master, datanode和 tasktracker 就应该对应到 slave.

### HDFS
### MapReduce
### Hadoop MapReduce 作业的生命周期
### 