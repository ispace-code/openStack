### 版本号规则
#### 版本格式中的三位分别表示：
* 主版本(major)升级，表示API不兼容，或者架构调整。
* 次版本(minor)升级，表示增加了新功能，或者大的优化。
* 小版本(micro)升级，表示修复问题，或者小的优化。
##### `所以：二位版本越大，表示功能越多。三位版本越大，表示越稳定。`

#### 版本发布节奏：
小版本(三位版本)BUG修复一到两周一个迭代，改完测完审完就发布，如果是用户紧急问题，只修一个BUG就发，小版本只做保守改动，不增加任何新功能。
大版本(二位版本)和小版本并行开发，发布时会包含上一大版本的所有变更，小版本的所有变更会实时同步到大版本上。

------



### Git 与 SVN
#### Git
[Git](https://git-scm.com/) 是一个开源的分布式版本控制系统，用于敏捷高效地处理任何或小或大的项目。
Git 是 Linus Torvalds 为了帮助管理 Linux 内核开发而开发的一个开放源码的版本控制软件。
Git 与常用的版本控制工具 CVS, Subversion 等不同，它采用了分布式版本库的方式，不必服务器端软件支持。

?> `Git 是一套内容寻址(content-addressable)文件系统，一个key-Value数据库，在此之上提供了一个VCS 用户界面。`

#### Git 特点
* 速度快
* 只关心整体，直接快照
* 非线性开发强力支持，允许千个并行分支
* 完全分布式，本地几乎可以完成全部操作
* SHA-1 算法计算数据的校验和，数据库索引

#### Git主要存储对象
* Blob对象——二进制数据，共享相同文件，用来存储文件内容，或者说表示一个文件。
* Tree 对象——目录关系数据，指向tree、blob
* Commit对象——提交数据，指向tree（1个）
* 基于 key-value 数据存储，它允许插入任意类型的内容，并会返回一个键值，通过该键值可以在任何时候再取出该内容

![](../_media/git_core.png)

?> Git本质上是一个commit 对象的链表

?> Git 中的分支，是一个指向commit 对象的可变指针。
仅是一个包含所指对象校验和（40 个字符长度SHA-1 字串）的文件，所以创建和销毁一个分支就变得非常廉价。
新建一个分支就是向一个文件写入41 个字节（外加一个换行符）简单，所以速度很快

#### Git 工作流程

* 克隆 Git 资源作为工作目录。
* 在克隆的资源上添加或修改文件。
* 如果其他人修改了，你可以更新资源。
* 在提交前查看修改。
* 提交修改。
* 在修改完成后，如果发现错误，可以撤回提交并再次修改并提交。
![](../_media/git-process.png)

#### Git 工作区、暂存区和版本库
* 工作区：就是你在电脑里能看到的目录。
* 暂存区：英文叫stage, 或index。一般存放在 ".git目录下" 下的index文件（.git/index）中，所以我们把暂存区有时也叫作索引（index）。
* 版本库：工作区有一个隐藏目录.git，这个不算工作区，而是Git的版本库。
![](../_media/git-workspace.png)

#### Git 与 SVN 区别
1. Git 是分布式的，SVN 不是：这是 Git 和其它非分布式的版本控制系统，例如 SVN，CVS 等，最核心的区别。
2. Git 把内容按元数据方式存储，而 SVN 是按文件：所有的资源控制系统都是把文件的元信息隐藏在一个类似 .svn、.cvs 等的文件夹里。
3. Git 分支和 SVN 的分支不同：分支在 SVN 中一点都不特别，其实它就是版本库中的另外一个目录。
4. Git 没有一个全局的版本号，而 SVN 有：目前为止这是跟 SVN 相比 Git 缺少的最大的一个特征。
5. Git 的内容完整性要优于 SVN：Git 的内容存储使用的是 SHA-1 哈希算法。这能确保代码内容的完整性，确保在遇到磁盘故障和网络问题时降低对版本库的破坏。
![](../_media/git_svn.png)

### Git Flow 工作流
  `GitFlow`是一套基于Git的工作流程，这个工作流程围绕着项目发布定义了一个严格的如何建立分支的模型。
   `GitFlow`规定了如何建立、合并分支，如何发布，如何维护历史版本等工作流程。
![](../_media/gitflow.png)

#### Git Flow 优势
* 还处于半成品状态的feature不会影响到主干
* 各个开发人员之间做自己的分支，互不干扰
* 主干永远处于可编译、可运行的状态

#### Git Flow分支
* 主干分支（master和develop）
* 新功能分支(feature)
* 待发布分支(release)
* 维护补丁分支(hotfix)

#### Git Flow的命名约定
* 主分支名称：master
* 主开发分支名称：develop
* 标签（tag）名称：v##，如：v1.0.0
* 新功能开发分支名称：feature-## or feature/##，如：feature-games或feature/games
* 发布分支名称：release-## or release/##，如：release-1.0.0或release/1.0.0
* 维护分支名称：hotfix-## or hotfix/## ，如：hotfix-update或hotfix/update

#### Git Flow的工作流程
1. 创建develop分支
在本地master基础上创建一个develop分支，然后push到服务器
```git
git branch develop
git push -u origin develop
```

2. 新建feature分支
基于develop分支创建新功能分支
```git
git checkout -b feature/demo develop
git push
```

3. 完成新功能开发（合并feature分支到develop）
```git
git pull origin develop
git checkout develop
git merge --no-ff feature/demo
git push
git branch -d feature/demo
```
`新功能分支，永远不可以直接合并到master分支。合并可能会有冲突，应该谨慎处理冲突。`

4. 新建待发布分支release
项目准备发布时，基于develop分支新建一个待发布分支release,确立版本号
```git
git checkout -b release/v0.1 develop
git push
```

5. release分支合并到master发布
如果准备好了对外发布，就将release分支合并到master分支和develop分支上，并删除发布分支。
```git
//release分支合并到master分支
git checkout master
git merge --on-off release/v0.1
git push
//release分支合并到develop分支，合并完成后并删除发布分支
git checkout develop
git merge --on-off release/v0.1
git push
git branch -d release/v0.1
//合并到master分支，就应该打好Tag以方便跟踪。
git tag -a v0.1 -m 'Initial public release' master
git push --tags
```

6. 线上Bug修复流程
基于master新建hotfix分支
```git
git checkout -b hotfix/v0.1.0.1 master
```
将hotfix分支合并到master分支
```git
git checkout master
git merge --on-off hotfix/v0.1.0.1
git push
```
将hotfix分支合并到develop分支,合并完成后删除hotfix分支
```git
git checkout develop
git merge --on-off hotfix/v0.1.1
git push
git branch -d hotfix/v0.1.1
```
打标签
```git
git tag -a v0.1.1 -m 'Initial public release' master
git push --tags
```
!> Git 一般情况下不会丢失代码，除非checkout 的时候，checkout 命令执行完，指针已经变为最新分支，但是本地文件在IDE中，没有刷新过来。这个时候在IDE中编辑相关文件了，就有可能造成代码丢失，因为IDE 编辑的不是最新的文件内容。
### Git可视化工具
?>  [SourceTree](https://www.sourcetreeapp.com/) 软件
?>  [SmartGit]


### SVN
Apache Subversion 通常被缩写成 [SVN](https://subversion.apache.org/)，是一个开放源代码的版本控制系统，Subversion 在 2000 年由 CollabNet Inc 开发，现在发展成为 Apache 软件基金会的一个项目，同样是一个丰富的开发者和用户社区的一部分。
SVN相对于的RCS、CVS，采用了分支管理系统，它的设计目标就是取代CVS。互联网上免费的版本控制服务多基于Subversion。
#### SVN 生命周期
1. 创建版本库(Create)
2. 检出(Checkout)
3. 更新(update)
4. 执行变更(Rename)
5. 复查变化(Status)
6. 修复错误(revert)
7. 解决冲突(Merge)
8. 提交更改(Commit)
