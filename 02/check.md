### Code Style


### Java 代码规约插件
[https://github.com/alibaba/p3c](https://github.com/alibaba/p3c)
[https://p3c.alibaba.com/plugin/eclipse/update](https://p3c.alibaba.com/plugin/eclipse/update)


### Sonar 
[https://www.sonarqube.org/](https://www.sonarqube.org/)

### CheckStyle
[https://github.com/checkstyle/checkstyle](https://github.com/checkstyle/checkstyle)
[https://checkstyle.sourceforge.io/](https://checkstyle.sourceforge.io/)