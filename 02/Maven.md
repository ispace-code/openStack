
### Apache Maven 简介
![](../_media/maven-logo.png)
[Maven](http://maven.apache.org) 是 Apache 下的一个纯 Java 开发的开源项目。基于项目对象模型（缩写：POM）概念，Maven利用一个中央信息片断能管理一个项目的构建、报告和文档等步骤。
Maven 是一个项目管理工具，可以对 Java 项目进行构建、依赖管理。
Maven 也可被用于构建和管理各种项目，例如 C#，Ruby，Scala 和其他语言编写的项目。Maven 曾是 Jakarta 项目的子项目，现为由 Apache 软件基金会主持的独立 Apache 项目。
### Maven 安装
### Maven 使用

### Maven 核心概念
#### Maven 生命周期

Maven 的生命周期就是对所有的构建过程进行抽象和统一。包含了项目的清理、初始化、编译、测试、打包、集成测试、验证、部署和站点生成等几乎所有的构建步骤。
Maven 的生命周期是抽象的，即生命周期不做任何实际的工作，实际任务由插件完成，类似于设计模式中的模板方法。
Maven有三套相互独立的生命周期，分别是clean、default和site。每个生命周期包含一些阶段（phase），阶段是有顺序的，后面的阶段依赖于前面的阶段。
各个生命周期相互独立，一个生命周期的阶段前后依赖。

http://www.cnblogs.com/tenghoo/p/maven_life_cycle.html
http://www.yiibai.com/maven/maven_deployment_automation.html

##### 三套生命周期
1. clean生命周期：清理项目，包含三个phase。
```
1）pre-clean：执行清理前需要完成的工作
2）clean：清理上一次构建生成的文件
3）post-clean：执行清理后需要完成的工作
```
2. default生命周期：构建项目，重要的phase。
```
1）validate：验证工程是否正确，所有需要的资源是否可用。
2）compile：编译项目的源代码。  
3）test：使用合适的单元测试框架来测试已编译的源代码。这些测试不需要已打包和布署。
4）Package：把已编译的代码打包成可发布的格式，比如jar。
5）integration-test：如有需要，将包处理和发布到一个能够进行集成测试的环境。
6）verify：运行所有检查，验证包是否有效且达到质量标准。
7）install：把包安装到maven本地仓库，可以被其他工程作为依赖来使用。
8）Deploy：在集成或者发布环境下执行，将最终版本的包拷贝到远程的repository，使得其他的开发者或者工程可以共享。
```
3. site生命周期：建立和发布项目站点 phase。
```
1）pre-site：生成项目站点之前需要完成的工作
2）site：生成项目站点文档
3）post-site：生成项目站点之后需要完成的工作
4）site-deploy：将项目站点发布到服务器
```


#### Maven 依赖管理
Maven 一个核心的特性就是依赖管理。当我们处理多模块的项目（包含成百上千个模块或者子项目），模块间的依赖关系就变得非常复杂，管理也变得很困难。针对此种情形，Maven 提供了一种高度控制的方法。

##### 可传递性依赖发现
一种相当常见的情况，比如说 A 依赖于其他库 B。如果，另外一个项目 C 想要使用 A ，那么 C 项目也需要使用库 B。
Maven 可以避免去搜索所有所需库的需求。Maven 通过读取项目文件（pom.xml），找出它们项目之间的依赖关系。
我们需要做的只是在每个项目的 pom 中定义好直接的依赖关系。其他的事情 Maven 会帮我们搞定。
通过可传递性的依赖，所有被包含的库的图形会快速的增长。当有重复库时，可能出现的情形将会持续上升。Maven 提供一些功能来控制可传递的依赖的程度。

* `依赖调节` 决定当多个手动创建的版本同时出现时，哪个依赖版本将会被使用。 如果两个依赖版本在依赖树里的深度是一样的时候，第一个被声明的依赖将会被使用。
* `依赖管理` 直接的指定手动创建的某个版本被使用。例如当一个工程 C 在自己的依赖管理模块包含工程 B，即 B 依赖于 A， 那么 A 即可指定在 B 被引用时所使用的版本。
* `依赖范围` 包含在构建过程每个阶段的依赖。
* `依赖排除` 任何可传递的依赖都可以通过 "exclusion" 元素被排除在外。举例说明，A 依赖 B， B 依赖 C，因此 A 可以标记 C 为 "被排除的"。
* `依赖可选` 任何可传递的依赖可以被标记为可选的，通过使用 "optional" 元素。例如：A 依赖 B， B 依赖 C。因此，B 可以标记 C 为可选的， 这样 A 就可以不再使用 C。

##### 依赖元素
```xml
<dependency>  
    <groupId>org.springframework</groupId>  
    <artifactId>spring-core</artifactId>  
    <version>${springframework.version}</version>  
    <type>jar</type>  
    <scope>compile</scope>  
</dependency> 
```
* groupId,必选，实际隶属项目
* artifactId,必选，其中的模块
* version必选，版本号
* type可选，依赖类型，默认jar
* scope可选，依赖范围，默认compile
* optional可选，标记依赖是否可选，默认false
* exclusion可选，排除传递依赖性，默认空

##### 依赖范围
* `编译阶段(compile)` 该范围表明相关依赖是只在项目的类路径下有效。默认取值。
* `供应阶段(provided)` 该范围表明相关依赖是由运行时的 JDK 或者 网络服务器提供的。
* `运行阶段(runtime)` 该范围表明相关依赖在编译阶段不是必须的，但是在执行阶段是必须的。
* `测试阶段(test)` 该范围表明相关依赖只在测试编译阶段和执行阶段。
* `系统阶段(system)` 该范围表明你需要提供一个系统路径。
* `导入阶段(import)` 该范围只在依赖是一个 pom 里定义的依赖时使用。同时，当前项目的POM 文件的 部分定义的依赖关系可以取代某特定的 POM。 


#### Maven 继承
子模块pom文件继承父级模块pom 文件，父级pom文件中的依赖和变量，不必在子模块中重复定义。

#### Maven 聚合
模块化、子模块
#### Maven 仓库
* 本地仓库  {MAVEN_HOME}\conf\setting.xml文件中设置本地仓库目录和中央仓库账号设置
* 中央仓库
* 远程仓库
* 配置Nexus 私服
![](../_media/maven_nexus.png)

### Maven 插件
[Maven 常用命令与插件](09/Maven)
### Maven 常用命令
[Maven 常用命令与插件](09/Maven)