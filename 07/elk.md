


### ELK
ELK 不是一款软件，而是 Elasticsearch、Logstash 和 Kibana 三种软件产品的首字母缩写。这三者都是开源软件，通常配合使用。
ELK 已经成为目前最流行的集中式日志解决方案。
![](../_media/elk.png)

#### [Elasticsearch](/06/sousuo?id=elasticsearch)
[Elasticsearch](https://www.elastic.co/cn/products/elasticsearch) 分布式搜索和分析引擎，具有高可伸缩、高可靠和易管理等特点。基于 Apache Lucene 构建，能对大容量的数据进行接近实时的存储、搜索和分析操作。通常被用作某些应用的基础搜索引擎，使其具有复杂的搜索功能；
#### Logstash
[Logstash](https://www.elastic.co/cn/products/logstash) 数据收集引擎。它支持动态的从各种数据源搜集数据，并对数据进行过滤、分析、丰富、统一格式等操作，然后存储到用户指定的位置；
#### Filebeat
[Filebeat](https://www.elastic.co/cn/products/beats/filebeat) ELK 协议栈的新成员，一个轻量级开源日志文件数据搜集器，基于 Logstash-Forwarder 源代码开发，是对它的替代。在需要采集日志数据的 server 上安装 Filebeat，并指定日志目录或日志文件后，Filebeat 就能读取数据，迅速发送到 Logstash 进行解析，亦或直接发送到 Elasticsearch 进行集中式存储和分析。
#### Kafka
由于Logstash缺点是消耗系统资源比较大,运行时占用CPU和内存资源较高，所以一般使用Kafka消息队列做缓存，使用消息队列机制，缓解高并发，保障收集数据的安全性和稳定性。
[Kafka](/04/mq?id=kafka)
#### Kibana 
[Kibana](https://www.elastic.co/cn/products/kibana) 是一个基于Web的图形界面，用于搜索、分析和可视化存储在 Elasticsearch指标中的日志数据。通常与 Elasticsearch 配合使用，对其中数据进行搜索、分析和以统计图表的方式展示。
#### Grafana
[Grafana](https://grafana.com/) 是一个Go开发的、跨平台的开源的度量分析和可视化工具，可以通过将采集的数据查询然后可视化的展示，并及时通知。

### 分布式链路跟踪（APM）系统概况
* `CAT` CAT是大众点评开源的基于编码和配置的调用链分析，应用监控分析，日志采集，监控报警等一系列的监控平台工具。https://github.com/dianping/cat
* `PinPoint` 是韩国人开源的基于字节码注入的调用链分析，以及应用监控分析工具。特点是支持多种插件，UI功能强大，接入端无代码侵入。https://github.com/naver/pinpoint
* `Zipkin` 是Twitter开源的调用链分析工具，目前基于springcloud sleuth得到了广泛的使用，特点是轻量，使用部署简单。https://github.com/apache/incubator-zipkin
* `SkyWalking` 是国产开源的基于字节码注入的调用链分析，以及应用监控分析工具。特点是支持多种插件，UI功能较强，接入端无代码侵入。目前已加入Apache孵化器。https://github.com/apache/skywalking

### OpenTracing
OpenTracing是一个规范，开放式分布式追踪规范。目标是：解决不同的分布式追踪系统 API 不兼容的问题。
![](../_media/openstracing.png)

### Skywalking
[Skywalking](/08/Skywalking)
![](../_media/skywalking.png)
1578011967241