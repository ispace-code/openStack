### 虚拟机
#### 什么是虚拟机

虚拟机（VM）是计算机系统的仿真。简而言之，它可以在实际上是一台计算机的硬件上运行看起来很多单独的计算机。
操作系统（OS）及其应用程序从单个主机服务器或主机服务器池共享硬件资源。每个VM都需要自己的底层操作系统，并且硬件是虚拟化的。管理程序或虚拟机监视器是创建和运行VM的软件，固件或硬件。它位于硬件和虚拟机之间，是虚拟化服务器所必需的

流行的VM提供商
- VMware vSphere
- VirtualBox
- Xen
- Hyper-V
- KVM

#### OpenStack
#### Vmware 

### 容器
#### 什么是容器？

使用容器，而不是像虚拟机（VM）那样虚拟化底层计算机，只是虚拟化操作系统。
容器位于物理服务器及其主机操作系统之上 - 通常是Linux或Windows。每个容器共享主机操作系统内核，通常也包括二进制文件和库。共享组件是只读的。共享操作系统资源（如库）可以显着减少重现操作系统代码的需要，并且意味着服务器可以通过单个操作系统安装来运行多个工作负载。因此容器非常轻 - 它们只有几兆字节，只需几秒钟即可启动。与容器相比，VM需要几分钟才能运行，并且比同等容器大一个数量级。
与VM相比，容器所需的全部功能都足以支持程序和库以及运行特定程序的系统资源。实际上，这意味着您可以将容器上的应用程序的容量设置为使用容器的两到三倍，而不是使用VM。此外，使用容器，您可以为开发，测试和部署创建可移植，一致的操作环境。

![](../_media/01/container.jpg)

容器类型

- Linux容器（LXC） - 最初的Linux容器技术是Linux容器，通常称为LXC。LXC是一种Linux操作系统级虚拟化方法，用于在单个主机上运行多个隔离的Linux系统。
- Docker - Docker最初是作为一个构建单应用程序LXC容器的项目，向LXC引入了一些变化，使容器更易于使用和灵活使用。它后来变成了自己的容器运行时环境。在较高的层次上，Docker是一个可以高效创建，发布和运行容器的Linux实用程序。

热门容器供应商
- Linux容器
- LXC
- LXD
- CGManager
- docker
- Windows Server容器

容器的好处
1. 减少IT管理资源
2. 缩小了快照的大小
3. 更快地启动应用程序
4. 减少和简化安全更新
5. 减少传输，迁移，上传工作负载的代码

### 差异：虚拟机与容器虚拟机集装箱

容器 VS 全虚拟化
全虚拟化的系统分配到的是独有的一组资源，只有极少量的资源会共享，是有更强的隔离性，但是更加重了（需要更加多的资源）。用 Docker 容器有弱些的隔离性，但是它们更加轻量，需要更少的资源，所以你可以毫不费力地启动上千个容器。

基本上，Docker 容器和全虚拟化 VM 有着本质上不同的目标

![](../_media/01/diff.jpg)
对于大多数人来说，理想的设置可能包括两者。
利用当前的虚拟化技术状态，VM的灵活性和容器的最小资源需求协同工作，为环境提供最大的功能。

### Docker

Docker 是一个创建封装好的隔离计算机环境，每个封装好的环境都被称为容器。

![](../_media/01/docker_01.jpg)

启动一个 Docker 容器非常迅速，因为：每个容器共享宿主系统的内核。然而，各个容器都运行着一个 Linux 的副本，这意味着没有 hypervisor ，而且不需要额外的启动。对比之下，KVM, VirtualBox 或者 VMware 之类的虚拟机实现是不同的。

容器使应用具有可移植性，并能自包含
- 各容器共享着宿主机的内核 这意味着没有 hypervisor，而且不需要额外的系统启动
- 容器引擎负责启动或停止容器，这与虚拟机实现中的 hypervisor 类似。然而，容器中运行的进程与宿主系统的进程是同行级别的，所以不会被相关的 hypervisor 杀掉

#### K8s
#### sss