#### 介绍

?> 初入职场，正逢北京奥运会举办，有幸为国家奥运事业做出一点点事情，于玉渊潭望海楼参与 http://2008.cctv.com 项目，支持了奥运各项赛事的互联网报道。

?> 2013年加入搜狐汽车，从事中文分词和推荐引擎相关项目，为 http://ask.auto.sohu.com/ 提供问答推荐信息。

?> 2015年加入敦煌网 移动事业部，参与并推进了敦煌网移动端升级改造项目。由早期ejb项目向spring 服务化、模块化升级改造。

?> 2016年加入包商银行 数字银行事业部，主要负责有氧金融APP 渠道端工作。

?> 2017年开始主导有氧APP 和渠道端的架构升级，基于dubbo服务化完成个功能点的模块化。基础平台建设，
包括 SVN 向Git过渡，可持续构建jenkins。分布式组件 rocketmq建设。ELK、分布式链路跟踪与系统监控Skywalking等。

?> 至今。

#### 技术栈

```
基础工具
Git，Git-flow SourceTree,Jenkins,Maven,Nexus

前端
jQuery,VUE,Ant Design Vue,Element-UI-VUE

后端
Spring,SpringBoot,SpringCloud,Dubbo,Spring Cloud for Alibaba,RPC SOA

Redis,memcache,mongodb

mysql,orcale,mybatis,hibernate

Elasticsearch、Logstash、Kibana、Kafka、grafana、Skywalking APM

Zookeeper、RocketMQ
```

#### 当前工作
![输入图片说明](../_media/weixin.jpg "微信二维码.jpg")