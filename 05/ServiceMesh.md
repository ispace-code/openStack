### ServiceMesh
#### 服务网格
服务网格是一个用于处理服务间通信的基础设施层，它负责为构建复杂的云原生应用传递可靠的网络请求。在实践中，服务网格通常实现一组和应用程序部署在一起的轻量级的网络代理，但对应用程序来说是透明的。

Service Mesh 是服务的前置代理层的实现，采用 sidecar 设计模式，用来管理 inbound 和 outbound 流量，并且针对拦截的流量和具体配置，实现路由转发，策略控制，认证授权，数据监测等功能。将与业务服务紧密结合的外围支撑组件从服务组件中剥离，形成独立的基础设施层，进而让服务回归业务本身，不再考虑外围支撑，实现真正的服务无关性、无侵入式治理。

Service Mesh是层次化、规范化、体系化、无侵入的分布式服务治理技术平台。
* `层次化` 分为数据面和控制面两个概念，数据面是指所有数据流动的那个层面，控制面是用来控制这个数据面的，对服务去做处理。对数据面和控制面进行分层，带来的好处是，针对一个复杂的系统进行切分，可以获得更清晰的认识，这和devide and conque是同一个理念。
* `规范化` 是指通过标准协议完成数据平面和控制平面的连接，同时，sidecar成为所有traffic互联、互通的约束标准。
* `体系化` 包含两个维度，一是指observability全局考虑。目前在整个分布式治理过程中的最大挑战是：logging、metrics、tracing这三个observability领域的核心内容缺少体系性的关注。另一个是集中管理的维度，包括服务管理、限流、熔断、安全、灰度在内的服务模块都可以在获得体系化的呈现，每个服务都可以被看到，而非团队a只看限流，团队b只看logging，需要一种技术能力拉通所有的服务模块，这个体系化这个角度看，Service Mesh是一个理想的技术方案。
* `无侵入` 是指我们希望通过无侵入，当新增一个业务的时候，不需要考虑一个SDK去初始化，而是可以通过sidecar的进程方式来解耦。



#### Linkerd
[Linkerd](https://github.com/linkerd/linkerd) 是 Service Mesh（服务网格）技术的一个实现。Linkerd 基于 Java，所以运行时必须有 JVM 支持。

[`https://github.com/linkerd/linkerd`](https://github.com/linkerd/linkerd)
[`https://github.com/linkerd/linkerd2`](https://github.com/linkerd/linkerd2)

#### Istio
[`https://github.com/istio/istio`](https://github.com/istio/istio)

主要功能
1. 连接（Connect）：智能控制服务之间的调用流量，能够实现灰度升级、AB 测试和红黑部署等功能
2. 安全加固（Secure）：自动为服务之间的调用提供认证、授权和加密。
3. 控制（Control）：应用用户定义的 policy，保证资源在消费者中公平分配。
4. 观察（Observe）：查看服务运行期间的各种数据，比如日志、监控和 tracing，了解服务的运行情况。

![](../_media/istio.png)

Istio 是 Service Mesh 架构的一种实现，服务之间的通信（比如这里的 Service A 访问 Service B）会通过代理（默认是 Envoy）来进行。

* `Pilot`：为 Envoy 提供了服务发现，流量管理和智能路由（AB 测试、金丝雀发布等），以及错误处理（超时、重试、熔断）功能。 用户通过 Pilot 的 API 管理网络相关的资源对象，Pilot 会根据用户的配置和服务的信息把网络流量管理变成 Envoy 能识别的格式分发到各个 Sidecar 代理中。
* `Mixer`：为整个集群执行访问控制（哪些用户可以访问哪些服务）和 Policy 管理（Rate Limit，Quota 等），并且收集代理观察到的服务之间的流量统计数据。
* `Citadel`：为服务之间提供认证和证书管理，可以让服务自动升级成 TLS 协议。

#### SOFAMesh
[SOFAMesh](https://www.sofastack.tech/projects/sofa-mesh/overview/) 是基于 Istio 改进和扩展而来的 Service Mesh 大规模落地实践方案 。
[`https://github.com/sofastack/sofa-mesh`](https://github.com/sofastack/sofa-mesh)
[`https://github.com/sofastack/sofa-mosn`](https://github.com/sofastack/sofa-mosn)

### Spring Cloud体系与Service Mesh

#### 功能重叠
Service Mesh 设计理论上很高端，但实际上与现有SpringCloud框架功能重复。但Service Mesh 更关注多语言服务化。

| **功能**       | **Spring Cloud**                                             | **Isito**                                                    |
| -------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 服务注册与发现 | 支持，基于Eureka，consul等组件，提供server，和Client管理     | 支持，基于XDS接口获取服务信息，并依赖“虚拟服务路由表”实现服务发现 |
| 链路监控       | 支持，基于Zikpin或者Pinpoint或者Skywalking实现               | 支持，基于sideCar代理模型，记录网络请求信息实现              |
| API网关        | 支持，基于zuul或者spring-cloud-gateway实现                   | 支持，基于Ingress gateway以及egress实现                      |
| 熔断器         | 支持，基于Hystrix实现                                        | 支持，基于声明配置文件，最终转化成路由规则实现               |
| 服务路由       | 支持，基于网关层实现路由转发                                 | 支持，基于iptables规则实现                                   |
| 安全策略       | 支持，基于spring-security组件实现，包括认证，鉴权等，支持通信加密 | 支持，基于RBAC的权限模型，依赖Kubernetes实现，同时支持通信加密 |
| 配置中心       | 支持，springcloud-config组件实现                             | 不支持                                                       |
| 性能监控       | 支持，基于Spring cloud提供的监控组件收集数据，对接第三方的监控数据存储 | 支持，基于SideCar代理，记录服务调用性能数据，并通过metrics adapter，导入第三方数据监控工具 |
| 日志收集       | 支持，提供client，对接第三方日志系统，例如ELK                | 支持，基于SideCar代理，记录日志信息，并通过log adapter，导入第三方日志系统 |
| 工具客户端集成 | 支持，提供消息，总线，部署管道，数据处理等多种工具客户端SDK  | 不支持                                                       |
| 分布式事务     | 支持，支持不同的分布式事务模式：JTA，TCC，SAGA等，并且提供实现的SDK框架 | 不支持                                                       |



#### 服务容器化
#### 术业有专攻
#### 语言壁垒