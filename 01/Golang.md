### Go
![](../_media/go.png)
[Go](https://github.com/golang/go) （又称Golang）是Google开发的一种`静态强类型、编译型、并发型，并具有垃圾回收功能`的编程语言。
罗伯特·格瑞史莫（Robert Griesemer），罗勃·派克（Rob Pike）及肯·汤普逊（Ken Thompson）于2007年9月开始设计Go，稍后Ian Lance Taylor、Russ Cox加入项目。Go是基于Inferno操作系统所开发的。Go于2009年11月正式宣布推出，成为开放源代码项目，并在Linux及Mac OS X平台上进行了实现，后来追加了Windows系统下的实现。

[LiteIDE X](http://liteide.org/cn/)  [`https://github.com/visualfc/liteide`](https://github.com/visualfc/liteide)

#### Go 是编译型语言
Go 使用编译器来编译代码。编译器将源代码编译成二进制（或字节码）格式；在编译代码时，编译器检查错误、优化性能并输出可在不同平台上运行的二进制文件。要创建并运行 Go 程序，程序员必须执行如下步骤。
使用文本编辑器创建 Go 程序；
保存文件；
编译程序；
运行编译得到的可执行文件。

这不同于 Python、Ruby 和 JavaScript 等语言，它们不包含编译步骤。Go 自带了编译器，因此无须单独安装编译器。

#### 编译器
当前有两个Go编译器分支，分别为官方编译器gc和gccgo。官方编译器在初期使用C写成，后用Go重写从而实现自举。Gccgo是一个使用标准GCC作为后端的Go编译器。
官方编译器支持跨平台编译（但不支持CGO），允许将源代码编译为可在目标系统、架构上执行的二进制文件。

#### 集成开发环境（IDE，Integrated Development Environment ）
* [GoLand](https://www.jetbrains.com/go/)
* [LiteIDE](https://github.com/visualfc/liteide)
* [Eclipse](https://github.com/GoClipse/goclipse)

#### Go语言为并发而生
在早期 CPU 都是以单核的形式顺序执行机器指令。Go语言的祖先C语言正是这种顺序编程语言的代表。顺序编程语言中的顺序是指：所有的指令都是以串行的方式执行，在相同的时刻有且仅有一个 CPU 在顺序执行程序的指令。
随着处理器技术的发展，单核时代以提升处理器频率来提高运行效率的方式遇到了瓶颈，单核 CPU 发展的停滞，给多核 CPU 的发展带来了机遇。相应地，编程语言也开始逐步向并行化的方向发展。
虽然一些编程语言的框架在不断地提高多核资源使用效率，例如 Java 的 Netty 等，但仍然需要开发人员花费大量的时间和精力搞懂这些框架的运行原理后才能熟练掌握。
作为程序员，要开发出能充分利用硬件资源的应用程序是一件很难的事情。现代计算机都拥有多个核，但是大部分编程语言都没有有效的工具让程序可以轻易利用这些资源。编程时需要写大量的线程同步代码来利用多个核，很容易导致错误。
Go语言正是在多核和网络化的时代背景下诞生的原生支持并发的编程语言。Go语言从底层原生支持并发，无须第三方库，开发人员可以很轻松地在编写程序时决定怎么使用 CPU 资源。
Go语言的并发是基于 goroutine 的，goroutine 类似于线程，但并非线程。可以将 goroutine 理解为一种虚拟线程。Go语言运行时会参与调度 goroutine，并将 goroutine 合理地分配到每个 CPU 中，最大限度地使用 CPU 性能。
多个 goroutine 中，Go语言使用通道（channel）进行通信，通道是一种内置的数据结构，可以让用户在不同的 goroutine 之间同步发送具有类型的消息。这让编程模型更倾向于在 goroutine 之间发送消息，而不是让多个 goroutine 争夺同一个数据的使用权。

#### Go语言标准库常用的包及功能

| **Go语言标准库包名** | **功  能**                                                   |
| -------------------- | ------------------------------------------------------------ |
| bufio                | 带缓冲的 I/O 操作                                            |
| bytes                | 实现字节操作                                                 |
| container            | 封装堆、列表和环形列表等容器                                 |
| crypto               | 加密算法                                                     |
| database             | 数据库驱动和接口                                             |
| debug                | 各种调试文件格式访问及调试功能                               |
| encoding             | 常见算法如 JSON、XML、Base64 等                              |
| flag                 | 命令行解析                                                   |
| fmt                  | 格式化操作                                                   |
| go                   | Go语言的词法、语法树、类型等。可通过这个包进行代码信息提取和修改 |
| html                 | HTML 转义及模板系统                                          |
| image                | 常见图形格式的访问及生成                                     |
| io                   | 实现 I/O 原始访问接口及访问封装                              |
| math                 | 数学库                                                       |
| net                  | 网络库，支持 Socket、HTTP、邮件、RPC、SMTP 等                |
| os                   | 操作系统平台不依赖平台操作封装                               |
| path                 | 兼容各操作系统的路径操作实用函数                             |
| plugin               | Go 1.7 加入的插件系统。支持将代码编译为插件，按需加载        |
| reflect              | 语言反射支持。可以动态获得代码中的类型信息，获取和修改变量的值 |
| regexp               | 正则表达式封装                                               |
| runtime              | 运行时接口                                                   |
| sort                 | 排序接口                                                     |
| strings              | 字符串转换、解析及实用函数                                   |
| time                 | 时间接口                                                     |
| text                 | 文本模板及 Token 词法器                                      |
|                      |                                                              |

#### Go语言工程结构
一个Go语言项目的目录一般包含以下三个子目录：
* src 目录：放置项目和库的源文件；
* pkg 目录：放置编译后生成的包/库的归档文件；
* bin 目录：放置编译后生成的可执行文件。
* vendor 目录：Go语言从 1.5 版本开始开始引入 vendor 模式，如果项目目录下有 vendor 目录，那么Go语言编译器会优先使用 vendor 内的包进行编译、测试等。

#### Go 项目依赖管理工具
##### Go Modules （Golang包管理工具）
##### Go依赖管理工具 - dep

https://github.com/snail007/goproxy

https://github.com/ouqiang/goproxy
#### Go 中的默认规则
##### 大写字母导出机制
##### 下划线


https://github.com/miaolz123/vue-markdown
