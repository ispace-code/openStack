### Java 体系
* JavaSE (J2SE)(Java 2 Platform Standard Edition，Java平台标准版）
* JavaEE (J2EE)(Java 2 Platform,Enterprise Edition，Java平台企业版)
* JavaME (J2ME)(Java 2 Platform Micro Edition，Java平台微型版)

### Java 语言基础
 #### Java 关键字

| 类别                 | 关键字    | 说明     |
|----------------------|--------   |------    |
| 访问控制             | private   | 是有的   |
|                      | protected | 受保护的 |
|                      | public    | 公共的   |
| 类、方法和变量修饰符 | abstract  | 声明抽象 |
|                      | class     | 类       |
|                      | extends   | 扩充,继承 |
|                      | final     | 最终值,不可改变的 |
|                      | implements| 实现（接口） |
|                      | interface | 接口 |
|                      | native    | 本地，原生方法（非 Java 实现） |
|                      | new       | 新,创建 |
|                      | static    | 静态 |
|                      | strictfp  | 严格,精准 |
|                      | synchronized | 线程,同步 |
|                      | transient    | 短暂 |
|                      | volatile     | 易失 |
| 程序控制语句         | break        | 跳出循环  |
|                      | case         | 定义一个值以供 switch 选择|
|                      | continue     | 继续 |
|                      | default      | 默认 |
|                      | do           | 运行 |
|                      | else         | 否则 |
|                      | for          | 循环 |
|                      | if           | 如果 |
|                      | instanceof   | 实例 | 
|                      | return       | 返回 |
|                      | switch       | 根据值选择执行 |
|                      | while        | 循环 |
| 错误处理             | assert       |断言表达式是否为真|
|                      | catch        |捕捉异常|
|                      | finally      |有没有异常都执行|
|                      | throw        |抛出一个异常对象|
|                      | throws       |声明一个异常可能被抛出|
|                      | try          |捕获异常|
| 包相关               | import       |引入|
|                      | package      | 包 |
|  基本类型            | boolean      |布尔型|
|                      | byte         |字节型|
|                      | char         |字符型|
|                      | double       |双精度浮点|
|                      | float        |单精度浮点|
|                      | int          |整型|
|                      | long         |长整型|
|                      | short        |短整型|
| 变量引用             | super        |父类,超类|
|                      | this         |本类|
|                      | void         |无返回值|
| 保留关键字           | goto         |是关键字，但不能使用|
|                      | const        |是关键字，但不能使用|
|                      | null         |空|

 #### 基本数据类型
 ##### byte
```
byte 数据类型是8位、有符号的，以二进制补码表示的整数；
最小值是 -128（-2^7）；
最大值是 127（2^7-1）；
默认值是 0；
byte 类型用在大型数组中节约空间，主要代替整数，因为 byte 变量占用的空间只有 int 类型的四分之一；
例子：byte a = 100，byte b = -50。
```

 ##### short
```
short 数据类型是 16 位、有符号的以二进制补码表示的整数
最小值是 -32768（-2^15）；
最大值是 32767（2^15 - 1）；
Short 数据类型也可以像 byte 那样节省空间。一个short变量是int型变量所占空间的二分之一；
默认值是 0；
例子：short s = 1000，short r = -20000。
```

 ##### int
```
int 数据类型是32位、有符号的以二进制补码表示的整数；
最小值是 -2,147,483,648（-2^31）；
最大值是 2,147,483,647（2^31 - 1）；
一般地整型变量默认为 int 类型；
默认值是 0 ；
例子：int a = 100000, int b = -200000。
```

 ##### long
```
long 数据类型是 64 位、有符号的以二进制补码表示的整数；
最小值是 -9,223,372,036,854,775,808（-2^63）；
最大值是 9,223,372,036,854,775,807（2^63 -1）；
这种类型主要使用在需要比较大整数的系统上；
默认值是 0L；
例子： long a = 100000L，Long b = -200000L。
"L"理论上不分大小写，但是若写成"l"容易与数字"1"混淆，不容易分辩。所以最好大写。
```

 ##### float
```
float 数据类型是单精度、32位、符合IEEE 754标准的浮点数；
float 在储存大型浮点数组的时候可节省内存空间；
默认值是 0.0f；
浮点数不能用来表示精确的值，如货币；
例子：float f1 = 234.5f。
```

 ##### double
```
double 数据类型是双精度、64 位、符合IEEE 754标准的浮点数；
浮点数的默认类型为double类型；
double类型同样不能表示精确的值，如货币；
默认值是 0.0d；
例子：double d1 = 123.4。
```

 ##### boolean
```
boolean数据类型表示一位的信息；
只有两个取值：true 和 false；
这种类型只作为一种标志来记录 true/false 情况；
默认值是 false；
例子：boolean one = true。
```

 ##### char 
```
char类型是一个单一的 16 位 Unicode 字符；
最小值是 \u0000（即为0）；
最大值是 \uffff（即为65,535）；
char 数据类型可以储存任何字符；
例子：char letter = 'A';。
```
 #### 变量常量
 ##### 在Java语言中，所有的变量在使用前必须声明。声明变量的基本格式如下：
```java
type identifier [ = value][, identifier [= value] ...] ;

int a, b, c;         // 声明三个int型整数：a、 b、c
int d = 3, e = 4, f = 5; // 声明三个整数并赋予初值
byte z = 22;         // 声明并初始化 z
String s = "runoob";  // 声明并初始化字符串 s
double pi = 3.14159; // 声明了双精度浮点型变量 pi
char x = 'x';        // 声明变量 x 的值是字符 'x'。
```

 ##### Java语言支持的变量类型
* 类变量：独立于方法之外的变量，用 static 修饰。
* 实例变量：独立于方法之外的变量，不过没有 static 修饰。
* 局部变量：类的方法中的变量。


#### 引用数据类型
在Java中，引用类型的变量非常类似于C/C++的指针。引用类型指向一个对象，指向对象的变量是引用变量。这些变量在声明时被指定为一个特定的类型，比如Employee、Pubby等。变量一旦声明后，类型就不能被改变了。
对象、数组都是引用数据类型。
`所有引用类型的默认值都是null。`
一个引用变量可以用来引用与任何与之兼容的类型。
```java
例子：Site site = new Site("Runoob")。
```
 #### 运算符
##### 算术运算符
```
  +  加法: 相加运算符两侧的值	A + B 等于 30
  -  减法: 左操作数减去右操作数	A – B 等于 -10
  *  乘法: 相乘操作符两侧的值	A * B等于200
  /  除法: 左操作数除以右操作数	B / A等于2
  %  取模: 左操作数除右操作数的余数	B%A等于0
  ++ 自增: 操作数的值增加1	B++ 或 ++B 等于 21（区别详见下文）
  -- 自减: 操作数的值减少1	B-- 或 --B 等于 19（区别详见下文）
```
###### 自增自减运算符
`自增(++)自减(--)运算符是一种特殊的算术运算符,在算术运算符中需要两个操作数来进行运算,而自增自减运算符是一个操作数`

```java
public class selfAddMinus{
    public static void main(String[] args){
        int a = 3;//定义一个变量；
        int b = ++a;//自增运算
        int c = 3;
        int d = --c;//自减运算
        System.out.println("进行自增运算后的值等于"+b);
        System.out.println("进行自减运算后的值等于"+d);
    }
}
```
`(i++)表示运算完成之后增1,(++i)表示增1之后再运算`

##### 关系运算符
```
==	检查如果两个操作数的值是否相等，如果相等则条件为真。	（A == B）为假(非真)。
!=	检查如果两个操作数的值是否相等，如果值不相等则条件为真。	(A != B) 为真。
> 	检查左操作数的值是否大于右操作数的值，如果是那么条件为真。	（A> B）非真。
< 	检查左操作数的值是否小于右操作数的值，如果是那么条件为真。	（A <B）为真。
>=	检查左操作数的值是否大于或等于右操作数的值，如果是那么条件为真。	（A> = B）为假。
<=	检查左操作数的值是否小于或等于右操作数的值，如果是那么条件为真。	（A <= B）为真。
```
##### 位运算符
###### Java定义了位运算符，应用于整数类型(int)，长整型(long)，短整型(short)，字符型(char)，和字节型(byte)等类型。位运算符作用在所有的位上，并且按位运算。

```java
假设a = 60，b = 13;它们的二进制格式表示将如下：
A = 0011 1100
B = 0000 1101
-----------------
A&B = 0000 1100
A | B = 0011 1101
A ^ B = 0011 0001
~A= 1100 0011
```

```
&  	如果相对应位都是1，则结果为1，否则为0 (A＆B)，得到12，即0000 1100
|  	如果相对应位都是0，则结果为0，否则为1 (A | B)得到61，即 0011 1101
^  	如果相对应位值相同，则结果为0，否则为1 (A ^ B)得到49，即 0011 0001
~  	按位补运算符翻转操作数的每一位，即0变成1，1变成0。(~A)得到-61，即1100 0011
<< 	按位左移运算符。左操作数按位左移右操作数指定的位数。A << 2得到240，即 1111 0000
>> 	按位右移运算符。左操作数按位右移右操作数指定的位数。A >> 2得到15即 1111
>>>	按位右移补零操作符。左操作数的值按右操作数指定的位数右移，移动得到的空位以零填充。A>>>2得到15即0000 1111
```

##### 逻辑运算符
```
&& 称为逻辑与运算符。当且仅当两个操作数都为真,条件才为真。(A && B)为假。
|| 称为逻辑或操作符。如果任何两个操作数任何一个为真,条件为真。(A || B)为真。
!  称为逻辑非运算符。用来反转操作数的逻辑状态。如果条件为true,则逻辑非运算符将得到false。!(A && B)为真。
```
`当使用与逻辑运算符时，在两个操作数都为true时，结果才为true，但是当得到第一个操作为false时，其结果就必定是false，这时候就不会再判断第二个操作了。`

##### 赋值运算符
```
=    简单的赋值运算符，将右操作数的值赋给左侧操作数	C=A+B将把A+B得到的值赋给C
+=   加和赋值操作符，它把左操作数和右操作数相加赋值给左操作数	C+=A 等价于C=C+A
-=   减和赋值操作符，它把左操作数和右操作数相减赋值给左操作数	C-=A 等价于C=C-A
*=   乘和赋值操作符，它把左操作数和右操作数相乘赋值给左操作数	C*=A 等价于C=C*A
/=   除和赋值操作符，它把左操作数和右操作数相除赋值给左操作数	C/=A 等价于C=C/A
(%)= 取模和赋值操作符，它把左操作数和右操作数取模后赋值给左操作数	C%=A 等价于C=C%A
<<=  左移位赋值运算符   C<<=2 等价于C=C<<2
>>=  右移位赋值运算符   C>>=2 等价于C=C>>2
&=   按位与赋值运算符   C&=2 等价于C=C&2
^=   按位异或赋值操作符 C^=2 等价于C=C^2
|=   按位或赋值操作符   C|=2 等价于C=C|2
```

##### 其他运算符
###### 条件运算符
`条件运算符也被称为三元运算符。该运算符有3个操作数，并且需要判断布尔表达式的值。该运算符的主要是决定哪个值应该赋值给变量`
```java
variable x = (expression) ? value if true : value if false

public class Test {
   public static void main(String[] args){
      int a , b;
      a = 10;
      // 如果 a 等于 1 成立，则设置 b 为 20，否则为 30
      b = (a == 1) ? 20 : 30;
      System.out.println( "Value of b is : " +  b );
      // 如果 a 等于 10 成立，则设置 b 为 20，否则为 30
      b = (a == 10) ? 20 : 30;
      System.out.println( "Value of b is : " + b );
   }
}
```

###### instanceOf 运算符
`该运算符用于操作对象实例，检查该对象是否是一个特定类型（类类型或接口类型）`
```
( Object reference variable ) instanceof  (class/interface type)

String name = "James";
boolean result = name instanceof String; // 由于 name 是 String 类型，所以返回真
```

##### Java运算符优先级

| 类别 | 操作符 | 关联性 |
| ---- | ------ | ------ |
| 后缀 |() [] . (点操作符) |左到右|
| 一元 |+ + - !~|从右到左|
| 乘性 |* /% |左到右|
| 加性 |+ - |左到右|
| 移位 |>> >>>  << |左到右|
| 关系 |>> = << =  |左到右|
| 相等 |== != |左到右|
| 按位与 | & |左到右|
| 按位异或 | ^ |左到右|
| 按位或 | \| |左到右|
| 逻辑与 | && |左到右|
| 逻辑或 | \|\| |左到右|
| 条件 |?:|从右到左|
| 赋值 |= + = - = * = / =％= >> = << =＆= ^ = \| = |从右到左|
| 逗号 |，|左到右|

 #### 类与对象


 #### 循环结构
 ##### while 循环
 ###### while是最基本的循环
```java
while( 布尔表达式 ) {
  //循环内容
}
 ```
 ##### do…while 循环
 ###### 对于 while 语句而言，如果不满足条件，则不能进入循环。但有时候我们需要即使不满足条件，也至少执行一次。do…while 循环和 while 循环相似，不同的是，do…while 循环至少会执行一次。
```java
do {
       //代码语句
}while(布尔表达式);
 ```

 ##### for循环
 ###### 虽然所有循环结构都可以用 while 或者 do...while表示，但 Java 提供了另一种语句 —— for 循环，使一些循环结构变得更加简单。for循环执行的次数是在执行前就确定的。语法格式如下：
```java
for(初始化; 布尔表达式; 更新) {
    //代码语句
}
 ```
 ##### Java 增强 for 循环
 ###### Java5 引入了一种主要用于数组的增强型 for 循环。Java 增强 for 循环语法格式如下:
```java
for(声明语句 : 表达式) {
   //代码句子
}
 ```
 ##### break 关键字
 ###### break 的用法很简单，就是循环结构中的一条语句：
```java
public class Test {
  public static void main(String args[]) {
     int [] numbers = {10, 20, 30, 40, 50};
     for(int x : numbers ) {
       // x 等于 30 时跳出循环
        if( x == 30 ) {
           break;
        }
        System.out.print( x );
        System.out.print("\n");
     }
  }
}
 ```
 ##### continue 关键字
 ###### continue 适用于任何循环控制结构中。作用是让程序立刻跳转到下一次循环的迭代。在 for 循环中，continue 语句使程序立即跳转到更新语句。在 while 或者 do…while 循环中，程序立即跳转到布尔表达式的判断语句。
 ```java
public class Test {
   public static void main(String args[]) {
      int [] numbers = {10, 20, 30, 40, 50};
 
      for(int x : numbers ) {
         if( x == 30 ) {
        continue;
         }
         System.out.print( x );
         System.out.print("\n");
      }
   }
}
 ```

 #### 分支结构
 ##### Java 条件语句 - if...else
 ###### 一个 if 语句包含一个布尔表达式和一条或多条语句。
 ```java
if(布尔表达式) {
   //如果布尔表达式为true将执行的语句
}
 ```
 ###### if 语句后面可以跟 else 语句，当 if 语句的布尔表达式值为 false 时，else 语句块会被执行。
 ```java
if(布尔表达式){
   //如果布尔表达式的值为true
} else {
   //如果布尔表达式的值为false
}
 ```
 ###### if...else if...else 语句,
 ```java
if(布尔表达式 1) {
   //如果布尔表达式 1的值为true执行代码
} else if(布尔表达式 2){
   //如果布尔表达式 2的值为true执行代码
} else if(布尔表达式 3){
   //如果布尔表达式 3的值为true执行代码
} else {
   //如果以上布尔表达式都不为true执行代码
}
 ```

 ##### Java switch case 语句
 ###### switch case 语句判断一个变量与一系列值中某个值是否相等，每个值称为一个分支。
 ##### switch case 语句规则
* switch 语句中的变量类型可以是： byte、short、int 或者 char。从 Java SE 7 开始，switch 支持字符串 String 类型了，同时 case 标签必须为字符串常量或字面量。
* switch 语句可以拥有多个 case 语句。每个 case 后面跟一个要比较的值和冒号。
* case 语句中的值的数据类型必须与变量的数据类型相同，而且只能是常量或者字面常量。
* 当变量的值与 case 语句的值相等时，那么 case 语句之后的语句开始执行，直到 break 语句出现才会跳出 switch 语句。
* 当遇到 break 语句时，switch 语句终止。程序跳转到 switch 语句后面的语句执行。case 语句不必须要包含 break 语句。如果没有 break 语句出现，程序会继续执行下一条 case 语句，直到出现 break 语句。
* switch 语句可以包含一个 default 分支，该分支一般是 switch 语句的最后一个分支（可以在任何位置，但建议在最后一个）。default 在没有 case 语句的值和变量值相等的时候执行。default 分支不需要 break 语句。

```java
public class Test {
   public static void main(String args[]){
      //char grade = args[0].charAt(0);
      char grade = 'C'; 
      switch(grade)
      {
         case 'A' :
            System.out.println("优秀"); 
            break;
         case 'B' :
         case 'C' :
            System.out.println("良好");
            break;
         case 'D' :
            System.out.println("及格");
            break;
         case 'F' :
            System.out.println("你需要再努力努力");
            break;
         default :
            System.out.println("未知等级");
      }
      System.out.println("你的等级是 " + grade);
   }
}
```
 #### 数组
 ##### 数组声明定义
 ###### Java 语言中提供的数组是用来存储固定大小的同类型元素。数组对于每一门编程语言来说都是重要的数据结构之一，当然不同语言对数组的实现及处理也不尽相同。
```java
double[] myList;         // 首选的方法 
或 
double myList[];         //  效果相同，但不是首选方法
```
 ##### Arrays 类
 ###### java.util.Arrays 类能方便地操作数组，它提供的所有方法都是静态的。
* 给数组赋值：通过 fill 方法。
* 对数组排序：通过 sort 方法,按升序。
* 比较数组：通过 equals 方法比较数组中元素值是否相等。
* 查找数组元素：通过 binarySearch 方法能对排序好的数组进行二分查找法操作。


### Java 异常分类及处理 

#### 概念 

如果某个方法不能按照正常的途径完成任务，就可以通过另一种路径退出方法。在这种情况下会抛出一个封装了错误信息的对象。此时，这个方法会立刻退出同时不返回任何值。另外，调用
这个方法的其他代码也无法继续执行，异常处理机制会将代码执行交给异常处理器。 

![](../_media/01/img_100_51.png)

#### 异常分类 

Throwable是 Java 语言中所有错误或异常的超类。下一层分为 Error 和 Exception 
1. Error类是指 java运行时系统的内部错误和资源耗尽错误。应用程序不会抛出该类对象。如果出现了这样的错误，除了告知用户，剩下的就是尽力使程序安全的终止。 
2. Exception 又有两个分支，Exception（RuntimeException、CheckedException） 
 * RuntimeException 如：NullPointerException、 ClassCastException；
 * CheckedException，如 I/O 错误导致的 IOException、SQLException。

RuntimeException是那些可能在 Java 虚拟机正常运行期间抛出的异常的超类。 如果出现 RuntimeException，那么一
定是程序员的错误. 检查异常 CheckedException：一般是外部错误，这种异常都发生在编译阶段，Java 编译器会强制程序去捕获此类异常，即会出现要求你把这段可能出现异常的程序进行 try catch，该类异常一
般包括几个方面：  
1. 试图在文件尾部读取数据  
2. 试图打开一个错误格式的 URL  
3. 试图根据给定的字符串查找 class对象，而这个字符串表示的类并不存在  

#### 异常的处理方式 

遇到问题不进行具体处理，而是继续抛给调用者 （throw,throws） 
抛出异常有三种形式:
* throw
* throws
* 还有一种系统自动抛异常。 

```java
public static void main(String[] args) {  
	String s = "abc";  
	if(s.equals("abc")) {  
		throw new NumberFormatException();  
	} else {  
		System.out.println(s);  
	}
}  
int div(int a,int b) throws Exception {
	return a/b;
} 
```
try catch 捕获异常针对性处理方式 

#### Throw和 throws的区别

##### 位置不同 

1. throws 用在函数上，后面跟的是异常类，可以跟多个；而 throw 用在函数内，后面跟的是异常对象。  

##### 功能不同： 

2. throws 用来声明异常，让调用者只知道该功能可能出现的问题，可以给出预先的处理方式；throw抛出具体的问题对象，执行到 throw，功能就已经结束了，跳转到调用者，并将具体的问题对象抛给调用者。也就是说 throw 语句独立存在时，下面不要定义其他语句，因为执行不到。  
3. throws 表示出现异常的一种可能性，并不一定会发生这些异常；throw 则是抛出了异常，执行 throw则一定抛出了某种异常对象。 
4. 两者都是消极处理异常的方式，只是抛出或者可能抛出异常，但是不会由函数去处理异常，真正的处理异常由函数的上层调用处理。

### Java 反射

#### 动态语言 

动态语言，是指程序在运行时可以改变其结构：新的函数可以引进，已有的函数可以被删除等结构上的变化。比如常见的 JavaScript就是动态语言，除此之外 Ruby,Python等也属于动态语言，
而 C、C++则不属于动态语言。从反射角度说 JAVA属于半动态语言。

#### 反射机制概念 （运行状态中知道类所有的属性和方法）

在 Java 中的反射机制是指在运行状态中，对于任意一个类都能够知道这个类所有的属性和方法；并且对于任意一个对象，都能够调用它的任意一个方法；这种动态获取信息以及动态调用对象方法的功能成为 Java语言的反射机制。 

![](../_media/01/img_102_52.jpg)

#### 反射的应用场合 

##### 编译时类型和运行时类型 
在 Java程序中许多对象在运行是都会出现两种类型：`编译时类型`和`运行时类型` 
编译时的类型由声明对象时实用的类型来决定，运行时的类型由实际赋值给对象的类型决定 。
如： 
```java 
Java Person p=new Student();
```
其中编译时类型为 Person，运行时类型为 Student。的编译时类型无法获取具体方法程序在运行时还可能接收到外部传入的对象，该对象的编译时类型为 Object,但是程序有需要调用
该对象的运行时类型的方法。为了解决这些问题，程序需要在运行时发现对象和类的真实信息。然而，如果编译时根本无法预知该对象和类属于哪些类，程序只能依靠运行时信息来发现该对象
和类的真实信息，此时就必须使用到反射了。

#### Java反射 API

反射 API用来生成 JVM中的类、接口或则对象的信息。  
1. Class类：反射的核心类，可以获取类的属性，方法等信息。  
2. Field类：Java.lang.reflec 包中的类，表示类的成员变量，可以用来获取和设置类之中的属性值。  
3. Method类： Java.lang.reflec 包中的类，表示类的方法，它可以用来获取类中的方法信息或者执行方法。  
4. Constructor类： Java.lang.reflec 包中的类，表示类的构造方法。 

#### 反射使用步骤（获取 Class对象、调用对象方法） 

1. 获取想要操作的类的 Class对象，他是反射的核心，通过 Class对象我们可以任意调用类的方法。 
2. 调用 Class类中的方法，既就是反射的使用阶段。 
3. 使用反射 API来操作这些信息。

#### 获取 Class对象的 3种方法 

1. 调用某个对象的 getClass()方法 
```java
	Person p=new Person();
	Class clazz=p.getClass();
```
2. 调用某个类的 class属性来获取该类对应的 Class对象 
```java 
	Class clazz=Person.class; 
```
3. 使用 Class类中的 forName()静态方法(最安全/性能最好) 
```java
	Class clazz=Class.forName("类的全路径"); (最常用) 
	//当我们获得了想要操作的类的 Class对象后，可以通过 Class类中的方法获取并查看该类中的方法和属性。  
	//获取 Person类的 Class对象 
	Class clazz=Class.forName("reflection.Person");  
	//获取 Person类的所有方法信息 
	Method[] method=clazz.getDeclaredMethods(); 
	for(Method m:method){ 
		System.out.println(m.toString()); 
	} 
	//获取 Person类的所有成员属性信息 
	Field[] field=clazz.getDeclaredFields(); 
	for(Field f:field){ 
		System.out.println(f.toString()); 
	} 
	//获取 Person类的所有构造方法信息 
	Constructor[] constructor=clazz.getDeclaredConstructors(); 
	for(Constructor c:constructor){ 
		System.out.println(c.toString()); 
	}
```

#### 创建对象的两种方法 

1. 使用 Class 对象的 newInstance()方法来创建该 Class 对象对应类的实例，但是这种方法要求该 Class对象对应的类有默认的空构造器。调用 Constructor对象的 newInstance() 
2. 先使用 Class对象获取指定的 Constructor对象，再调用 Constructor对象的 newInstance()方法来创建 Class对象对应类的实例,通过这种方法可以选定构造方法创建实例。 

```java
	//获取 Person类的 Class对象 
	Class clazz=Class.forName("reflection.Person");  
	//使用.newInstane方法创建对象 
	Person p=(Person) clazz.newInstance(); 
   //获取构造方法并创建对象 
	Constructor c=clazz.getDeclaredConstructor(String.class,String.class,int.class); 
	//创建对象并设置属性 
	Person p1=(Person) c.newInstance("李四","男",20); 
```
### Java 注解

#### 概念 

Annotation（注解）是 Java 提供的一种对元程序中元素关联信息和元数据（metadata）的途径和方法。Annatation (注解)是一个接口，程序可以通过反射来获取指定程序中元素的 Annotation
对象，然后通过该 Annotation 对象来获取注解中的元数据信息。 

#### 4种标准元注解 

元注解的作用是负责注解其他注解。 Java5.0定义了 4个标准的 meta-annotation类型，它们被用来提供对其它 annotation类型作说明。 
* @Target 修饰的对象范围 
`@Target` 说明了 `Annotation` 所修饰的对象范围：
`Annotation` 可被用于 packages、types（类、接口、枚举、Annotation 类型）、类型成员（方法、构造方法、成员变量、枚举值）、方法参数和本地变量（如循环变量、catch参数）。
 在 `Annotation` 类型的声明中使用了 target 可更加明晰其修饰的目标 
* @Retention定义被保留的时间长短
`@Retention` 定义了该 Annotation被保留的时间长短：表示需要在什么级别保存注解信息，用于描述注解的生命周期（即：被描述的注解在什么范围内有效），取值（RetentionPoicy）由： 
 - SOURCE:在源文件中有效（即源文件保留） 
 - CLASS:在 class文件中有效（即 class保留） 
 - RUNTIME:在运行时有效（即运行时保留） 
* @Documented描述-javadoc 
`@Documented` 用于描述其它类型的 annotation 应该被作为被标注的程序成员的公共 API，因此可以被例如 javadoc此类的工具文档化。 
* @Inherited 阐述了某个被标注的类型是被继承的 
`@Inherited` 元注解是一个标记注解，`@Inherited` 阐述了某个被标注的类型是被继承的。如果一个使用了 `@Inherited` 修饰的 annotation 类型被用于一个 class，则这个 annotation 将被用于该class的子类。 

![](../_media/01/img_106_53.jpg)

#### 注解处理器

如果没有用来读取注解的方法和工作，那么注解也就不会比注释更有用处了。使用注解的过程中，很重要的一部分就是创建于使用注解处理器。Java SE5扩展了反射机制的API，以帮助程序员快速
的构造自定义注解处理器。下面实现一个注解处理器。 

1. 定义注解
```java
@Target(ElementType.FIELD) 
@Retention(RetentionPolicy.RUNTIME) 
@Documented 
public @interface FruitProvider { 
	/**供应商编号*/ 
	public int id() default -1; 
	/*** 供应商名称*/ 
	public String name() default ""； 
	/** * 供应商地址*/ 
	public String address() default ""; 
} 
```
2. 注解使用 
```java
public class Apple { 
    @FruitProvider(id = 1, name = "XXXXXX 集团", address = "XXXXXXXXX") 
    private String appleProvider; 
    public void setAppleProvider(String appleProvider) { 
        this.appleProvider = appleProvider; 
    } 
    public String getAppleProvider() { 
        return appleProvider; 
    } 
} 
```
3. 注解处理器
```java
public class FruitInfoUtil { 
	public static void getFruitInfo(Class<?> clazz) { 
		String strFruitProvicer = "供应商信息："; 
		Field[] fields = clazz.getDeclaredFields();//通过反射获取处理注解 
		for (Field field : fields) { 
			if (field.isAnnotationPresent(FruitProvider.class)) { 
				FruitProvider fruitProvider = (FruitProvider) field.getAnnotation(FruitProvider.class); 
				//注解信息的处理地方        
				strFruitProvicer = " 供应商编号：" + fruitProvider.id() + " 供应商名称：" 
					+ fruitProvider.name() + " 供应商地址："+ fruitProvider.address(); 
				System.out.println(strFruitProvicer); 
			} 
		} 
	} 
} 
public class FruitRun { 
    public static void main(String[] args) { 
        FruitInfoUtil.getFruitInfo(Apple.class); 
		/***********输出结果***************/ 
	}
} 
```

### Java 内部类

Java 类中不仅可以定义变量和方法，还可以定义类，这样定义在类内部的类就被称为内部类。根据定义的方式不同，内部类分为静态内部类，成员内部类，局部内部类，匿名内部类四种。 

#### 静态内部类

定义在类内部的静态类，就是静态内部类。 
```java
public class Out {
	private static int a;
	private int b;
	public static class Inner {
		public void print() {
			System.out.println(a);
		}
	}
}
```
1. 静态内部类可以访问外部类所有的静态变量和方法，即使是 private的也一样。 
2. 静态内部类和一般类一致，可以定义静态变量、方法，构造方法等。 
3. 其它类使用静态内部类需要使用“外部类.静态内部类”方式，如下所示：Out.Inner inner = new Out.Inner();inner.print(); 
4. Java集合类HashMap内部就有一个静态内部类Entry。Entry是HashMap存放元素的抽象，HashMap内部维护 Entry 数组用了存放元素，但是 Entry对使用者是透明的。像这种和外部类关系密切的，且不依赖外部类实例的，都可以使用静态内部类。

#### 成员内部类 

定义在类内部的非静态类，就是成员内部类。成员内部类不能定义静态方法和变量（final 修饰的
除外）。这是因为成员内部类是非静态的，类初始化的时候先初始化静态成员，如果允许成员内
部类定义静态变量，那么成员内部类的静态变量初始化顺序是有歧义的。 
```java
	public class Out {
		private static int a;
		private int b;
		public class Inner {
			public void print() {
				System.out.println(a);
				System.out.println(b);
			}
		}
	}
```
#### 局部内部类（定义在方法中的类） 

定义在方法中的类，就是局部类。如果一个类只在某个方法中使用，则可以考虑使用局部类。
```java
	public class Out {
		private static int a;
		private int b;
		public void test(final int c) {
			final int d = 1;
			class Inner {
				public void print() {
					System.out.println(c);
				}
			}
		}
	}
```

#### 匿名内部类（要继承一个父类或者实现一个接口、直接使用new来生成一个对象的引用）

匿名内部类我们必须要继承一个父类或者实现一个接口，当然也仅能只继承一个父类或者实现一个接口。同时它也是没有 class 关键字，这是因为匿名内部类是直接使用 new来生成一个对象的引
用。
```java
	public abstract class Bird {
		private String name;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		} 
		public abstract int fly();
	}
	public class Test {
		public void test(Bird bird){
			System.out.println(bird.getName() + "能够飞 " + bird.fly() + "米");
		}
		public static void main(String[] args) {
			Test test = new Test();
			test.test(new Bird() {
				public int fly() {
					return 10000;
				}
				public String getName() {
					return "大雁";
				}
			});
		}
	}
```

### Java 泛型

泛型提供了编译时类型安全检测机制，该机制允许程序员在编译时检测到非法的类型。泛型的本质是参数化类型，也就是说所操作的数据类型被指定为一个参数。比如我们要写一个排序方法，
能够对整型数组、字符串数组甚至其他任何类型的数组进行排序，我们就可以使用 Java 泛型。

#### 泛型方法（<E>） 

你可以写一个泛型方法，该方法在调用时可以接收不同类型的参数。根据传递给泛型方法的参数类型，编译器适当地处理每一个方法调用。 
``` java
	// 泛型方法 printArray
	public static < E > void printArray( E[] inputArray )
	{
		for ( E element : inputArray ){
			System.out.printf( "%s ", element );
		}
	}
```
1. <? extends T>表示该通配符所代表的类型是 T类型的子类。 
2. <? super T>表示该通配符所代表的类型是 T类型的父类。 

#### 泛型类<T> 

泛型类的声明和非泛型类的声明类似，除了在类名后面添加了类型参数声明部分。和泛型方法一样，泛型类的类型参数声明部分也包含一个或多个类型参数，参数间用逗号隔开。一个泛型参数，
也被称为一个类型变量，是用于指定一个泛型类型名称的标识符。因为他们接受一个或多个参数，这些类被称为参数化的类或参数化的类型。 
```java
	public class Box<T> {
		private T t;
		public void add(T t) {
			this.t = t;
		} 
		public T get() {
			return t;
		}
	}
```

#### 类型通配符 `?`

类型通配符一般是使用`?`代替具体的类型参数。例如 `List<?>` 在逻辑上是 List<String>,List<Integer> 等所有 List<具体类型实参>的父类。

#### 类型擦除

Java 中的泛型基本上都是在编译器这个层次来实现的。在生成的 Java 字节代码中是不包含泛型中的类型信息的。使用泛型的时候加上的类型参数，会被编译器在编译的时候去掉。这个
过程就称为类型擦除。如在代码中定义的 List<Object> 和 List<String>等类型，在编译之后都会变成 List。JVM看到的只是 List，而由泛型附加的类型信息对 JVM来说是不可见的。 
类型擦除的基本过程也比较简单，首先是找到用来替换类型参数的具体类。这个具体类一般是 Object。如果指定了类型参数的上界的话，则使用这个上界。把代码中的类型参数都替换
成具体的类。 

### Java 序列化(创建可复用的 Java 对象)

#### 保存(持久化)对象及其状态到内存或者磁盘

Java 平台允许我们在内存中创建可复用的 Java 对象，但一般情况下，只有当 JVM 处于运行时，这些对象才可能存在，即，这些对象的生命周期不会比 JVM 的生命周期更长。但在现实应用中，
就可能要求在JVM停止运行之后能够保存(持久化)指定的对象，并在将来重新读取被保存的对象。Java 对象序列化就能够帮助我们实现该功能。

#### 序列化对象以字节数组保持-静态成员不保存
使用 Java 对象序列化，在保存对象时，会把其状态保存为一组字节，在未来，再将这些字节组装成对象。必须注意地是，对象序列化保存的是对象的”状态”，即它的成员变量。由此可知，对
象序列化不会关注类中的静态变量。

#### 序列化用户远程对象传输
除了在持久化对象时会用到对象序列化之外，当使用 RMI(远程方法调用)，或在网络中传递对象时，都会用到对象序列化。Java序列化API为处理对象序列化提供了一个标准机制，该API简单易用。

#### Serializable 实现序列化
在 Java 中，只要一个类实现了 java.io.Serializable 接口，那么它就可以被序列化。ObjectOutputStream 和 ObjectInputStream 对对象进行序列化及反序列化
通过 ObjectOutputStream 和 ObjectInputStream 对对象进行序列化及反序列化。writeObject 和 readObject 自定义序列化策略在类中增加 writeObject 和 readObject 方法可以实现自定义序列化策略。

#### 序列化 ID
虚拟机是否允许反序列化，不仅取决于类路径和功能代码是否一致，一个非常重要的一点是两个类的序列化 ID 是否一致（就是 private static final long serialVersionUID）
序列化并不保存静态变量
序列化子父类说明
要想将父类对象也序列化，就需要让父类也实现 Serializable 接口。
Transient 关键字阻止该变量被序列化到文件中
1. 在变量声明前加上 Transient 关键字，可以阻止该变量被序列化到文件中，在被反序列化后，transient 变量的值被设为初始值，如 int 型的是 0，对象型的是 null。
2. 服务器端给客户端发送序列化对象数据，对象中有一些数据是敏感的，比如密码字符串等，希望对该密码字段在序列化时，进行加密，而客户端如果拥有解密的密钥，只有在客户端进行反序列化时，才可以对密码进行读取，这样可以一定程度保证序列化对象的数据安全。

### Java 复制

将一个对象的引用复制给另外一个对象，一共有三种方式。第一种方式是直接赋值，第二种方式是浅拷贝，第三种是深拷贝。这三种概念实际上都是为了拷贝对象。

#### 直接赋值复制

直接赋值。在 Java中，A a1 = a2，我们需要理解的是这实际上复制的是引用，也就是说 a1和 a2指向的是同一个对象。因此，当 a1变化的时候，a2里面的成员变量也会跟
着变化。 

#### 浅复制（复制引用但不复制引用的对象）

创建一个新对象，然后将当前对象的非静态字段复制到该新对象，如果字段是值类型的，那么对该字段执行复制；如果该字段是引用类型的话，则复制引用但不复制引用的对象。
因此，原始对象及其副本引用同一个对象。 
```java
class Resume  implements Cloneable{   
     public Object clone() {   
        try {   
            return (Resume)super.clone();   
        } catch (Exception e) {   
            e.printStackTrace();   
            return null;   
        }   
    }   
}   
```

#### 深复制（复制对象和其应用对象）

深拷贝不仅复制对象本身，而且复制对象包含的引用指向的所有对象。 
```java
class Student implements Cloneable { 
    String name; 
    int age; 
    Professor p; 
    Student(String name, int age, Professor p) { 
        this.name = name; 
        this.age = age; 
        this.p = p; 
    } 
     public Object clone() { 
        Student o = null; 
        try { 
            o = (Student) super.clone(); 
        } catch (CloneNotSupportedException e) { 
            System.out.println(e.toString()); 
        } 
        o.p = (Professor) p.clone(); 
        return o; 
    } 
} 
```

#### 序列化（深 clone 一中实现）

在 Java 语言里深复制一个对象，常常可以先使对象实现 Serializable 接口，然后把对象（实际上只是对象的一个拷贝）写到一个流里，再从流里读出来，便可以重建对象。 

### Java SPI