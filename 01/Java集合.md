### 接口继承关系和实现

集合类存放于 Java.util包中，主要有 3种：set(集）、list(列表包含Queue）和map(映射)。 

1. Collection：Collection是集合 List、Set、Queue的最基本的接口。 
2. Iterator：迭代器，可以通过迭代器遍历集合中的数据 
3. Map：是映射表的基础接口 

![](../_media/01/img_44_23.jpg)

![](../_media/01/img_45_24.jpg)


### LIST 

Java 的 List 是非常常用的数据类型。List 是有序的 Collection。Java List 一共三个实现类：
* ArrayList
* Vector
* LinkedList

![](../_media/01/img_46_25.jpg)

#### ArrayList（数组） 
ArrayList 是最常用的 List 实现类，内部是通过数组实现的，它允许对元素进行快速随机访问。数组的缺点是每个元素之间不能有间隔，当数组大小不满足时需要增加存储能力，就要将已经有数
组的数据复制到新的存储空间中。当从 ArrayList 的中间位置插入或者删除元素时，需要对数组进行复制、移动、代价比较高。因此，它适合随机查找和遍历，不适合插入和删除。 
#### Vector（数组实现、线程同步） 
Vector与 ArrayList一样，也是通过数组实现的，不同的是它支持线程的同步，即某一时刻只有一个线程能够写 Vector，避免多线程同时写而引起的不一致性，但实现同步需要很高的花费，因此，
访问它比访问 ArrayList慢。 
#### LinkList（链表） 
LinkedList是用链表结构存储数据的，很适合数据的动态插入和删除，随机访问和遍历速度比较慢。另外，他还提供了 List接口中没有定义的方法，专门用于操作表头和表尾元素，可以当作堆
栈、队列和双向队列使用。 

### SET 

Set注重独一无二的性质,该体系集合用于存储无序(存入和取出的顺序不一定相同)元素，值不能重复。对象的相等性本质是对象 hashCode值（java是依据对象的内存地址计算出的此序号）判断
的，如果想要让两个不同的对象视为相等的，就必须覆盖 Object的 hashCode方法和 equals方法。

![](../_media/01/img_47_26.jpg)

#### HashSet
哈希表边存放的是哈希值。HashSet存储元素的顺序并不是按照存入时的顺序（和 List显然不同） 而是按照哈希值来存的所以取数据也是按照哈希值取得。元素的哈希值是通过元素的
hashcode方法来获取的, HashSet首先判断两个元素的哈希值，如果哈希值一样，接着会比较equals方法 如果 equls结果为 true ，HashSet就视为同一个元素。如果 equals 为 false就不是
同一个元素。 哈希值相同 equals为 false的元素是怎么存储呢,就是在同样的哈希值下顺延（可以认为哈希值相同的元素放在一个哈希桶中）。也就是哈希一样的存一列。如图 1表示 hashCode值不相同的情
况；图 2表示 hashCode值相同，但 equals不相同的情况。 

![](../_media/01/img_48_27.png)

HashSet通过 hashCode值来确定元素在内存中的位置。一个 hashCode位置上可以存放多个元素。 

#### TreeSet（二叉树） 

1. TreeSet()是使用二叉树的原理对新 add()的对象按照指定的顺序排序（升序、降序），每增加一个对象都会进行排序，将对象插入的二叉树指定的位置。 
2. Integer和 String对象都可以进行默认的 TreeSet排序，而自定义类的对象是不可以的，自己定义的类必须实现 Comparable接口，并且覆写相应的 compareTo()函数，才可以正常使用。 
3. 在覆写 compare()函数时，要返回相应的值才能使 TreeSet按照一定的规则来排序 
4. 比较此对象与指定对象的顺序。如果该对象小于、等于或大于指定对象，则分别返回负整数、零或正整数。 

#### LinkHashSet（HashSet+LinkedHashMap） 
对于 LinkedHashSet 而言，它继承与 HashSet、又基于 LinkedHashMap 来实现的。LinkedHashSet 底层使用 LinkedHashMap 来保存所有元素，它继承与 HashSet，其所有的方法
操作上又与 HashSet相同，因此 LinkedHashSet 的实现上非常简单，只提供了四个构造方法，并通过传递一个标识参数，调用父类的构造器，底层构造一个 LinkedHashMap 来实现，在相关操
作上与父类 HashSet的操作相同，直接调用父类 HashSet的方法即可。 

### MAP 

![](../_media/01/img_49_28.jpg)

#### HashMap（数组+链表+红黑树）
哈希表（hash table）也叫散列表，是一种非常重要的数据结构，应用场景及其丰富，许多缓存技术（比如memcached）的核心其实就是在内存中维护一张大的哈希表，本文会对java集合框架中HashMap的实现原理进行讲解，并对JDK7的HashMap源码进行分析。

`HashMap 线程不安全，扩容的时候会造成数据访问不正确。`

HashMap根据键的 hashCode值存储数据，大多数情况下可以直接定位到它的值，因而具有很快的访问速度，但遍历顺序却是不确定的。 HashMap最多只允许一条记录的键为 null，允许多条记
录的值为 null。HashMap 非线程安全，即任一时刻可以有多个线程同时写 HashMap，可能会导致数据的不一致。如果需要满足线程安全，可以用 Collections 的 synchronizedMap 方法使
HashMap 具有线程安全的能力，或者使用 ConcurrentHashMap。我们用下面这张图来介绍 HashMap 的结构。 

`HashMap`由数组+链表组成的，数组是HashMap的主体，链表则是主要为了解决哈希冲突而存在的，如果定位到的数组位置不含链表（当前entry的next指向null）,那么查找，添加等操作很快，仅需一次寻址即可；如果定位到的数组包含链表，对于添加操作，其时间复杂度为O(n)，首先遍历链表，存在即覆盖，否则新增；对于查找操作来讲，仍需遍历链表，然后通过key对象的equals方法逐一比对查找。所以，性能考虑，HashMap中的链表出现越少，性能才会越好。
```java
//HashMap的主干数组，可以看到就是一个Entry数组，初始值为空数组{}，主干数组的长度一定是2的次幂。
transient Entry<K,V>[] table = (Entry<K,V>[]) EMPTY_TABLE;
```
Entry是HashMap中的一个静态内部类
```java
static class Entry<K,V> implements Map.Entry<K,V> {
  final K key;
  V value;
  Entry<K,V> next;//存储指向下一个Entry的引用，单链表结构
  int hash;//对key的hashcode值进行hash运算后得到的值，存储在Entry，避免重复计算

  /**
   * Creates new entry.
   */
  Entry(int h, K k, V v, Entry<K,V> n) {
    value = v;
    next = n;
    key = k;
    hash = h;
} 
......
```
`HashMap的总体结构`
![](../_media/hashmap.png)

HashMap内存储数据的Entry数组默认是16，如果没有对Entry扩容机制的话，
当存储的数据一多，Entry内部的链表会很长，这就失去了HashMap的存储意义了。所以HasnMap内部有自己的扩容机制。
HashMap内部有：
变量size，它记录HashMap的底层数组中已用槽的数量；
变量threshold，它是HashMap的阈值，用于判断是否需要调整HashMap的容量（threshold = 容量*加载因子）    
变量DEFAULT_LOAD_FACTOR = 0.75f，默认加载因子为0.75
HashMap扩容的条件是：当size大于threshold时，对HashMap进行扩容 
扩容是是新建了一个HashMap的底层数组，而后调用transfer方法，将就HashMap的全部元素添加到新的HashMap中（要重新计算元素在新的数组中的索引位置）。 很明显，扩容是一个相当耗时的操作，因为它需要重新计算这些元素在新的数组中的位置并进行复制处理。因此，我们在用HashMap的时，最好能提前预估下HashMap中元素的个数，这样有助于提高HashMap的性能。


##### JAVA7实现 
大方向上，HashMap 里面是一个数组，然后数组中每个元素是一个单向链表。上图中，每个绿色的实体是嵌套类 Entry 的实例，Entry 包含四个属性：key, value, hash 值和用于单向链表的 next。 
1. capacity：当前数组容量，始终保持 2^n，可以扩容，扩容后数组大小为当前的 2 倍。 
2. loadFactor：负载因子，默认为 0.75。 
3. threshold：扩容的阈值，等于 capacity * loadFactor 

![](../_media/01/img_49_29.png)

##### JAVA8实现  
Java8 对 HashMap 进行了一些修改，最大的不同就是利用了红黑树，所以其由 数组+链表+红黑树 组成。 
根据 Java7 HashMap 的介绍，我们知道，查找的时候，根据 hash 值我们能够快速定位到数组的具体下标，但是之后的话，需要顺着链表一个个比较下去才能找到我们需要的，时间复杂度取决
于链表的长度，为 O(n)。为了降低这部分的开销，在 Java8 中，当链表中的元素超过了 8 个以后，会将链表转换为红黑树，在这些位置进行查找的时候可以降低时间复杂度为 O(logN)。

![](../_media/01/img_50_30.png)

![](../_media/hashmap_put.png)

#### ConcurrentHashMap(`线程安全`)
 `ConcurrentHashMap 它引入了一个“分段锁”的概念，` 用以解决线程安全问题。 

##### Segment段 

ConcurrentHashMap 和 HashMap 思路是差不多的，但是因为它支持并发操作，所以要复杂一些。整个 ConcurrentHashMap 由一个个 Segment 组成，Segment 代表”部分“或”一段“的
意思，所以很多地方都会将其描述为分段锁。注意，行文中，我很多地方用了“槽”来代表一个 segment。 

##### 线程安全（Segment 继承 ReentrantLock 加锁） 

简单理解就是，ConcurrentHashMap 是一个 Segment 数组，Segment 通过继承ReentrantLock 来进行加锁，所以每次需要加锁的操作锁住的是一个 segment，这样只要保证每
个 Segment 是线程安全的，也就实现了全局的线程安全。 

![](../_media/01/img_51_32.png)

##### 并行度（默认 16） 

concurrencyLevel：并行级别、并发数、Segment 数，怎么翻译不重要，理解它。默认是 16，也就是说 ConcurrentHashMap 有 16 个 Segments，所以理论上，这个时候，最多可以同时支
持 16 个线程并发写，只要它们的操作分别分布在不同的 Segment 上。这个值可以在初始化的时候设置为其他值，但是一旦初始化以后，它是不可以扩容的。再具体到每个 Segment 内部，其实
每个 Segment 很像之前介绍的 HashMap，不过它要保证线程安全，所以处理起来要麻烦些。 

##### Java8实现 （引入了红黑树） 

Java8 对 ConcurrentHashMap 进行了比较大的改动,Java8 也引入了红黑树。

![](../_media/01/img_51_31.png)

### HashTable(`线程安全`)

`Hashtable 线程安全很好理解，因为它每个方法中都加入了Synchronize。`
Hashtable 是遗留类，很多映射的常用功能与 HashMap 类似，不同的是它承自 Dictionary 类，并且是线程安全的，任一时间只有一个线程能写 Hashtable，并发性不如 ConcurrentHashMap，
因为 ConcurrentHashMap 引入了分段锁。Hashtable 不建议在新代码中使用，不需要线程安全的场合可以用 HashMap替换，需要线程安全的场合可以用 ConcurrentHashMap替换

### TreeMap（可排序） 

TreeMap 实现 SortedMap 接口，能够把它保存的记录根据键排序，默认是按键值的升序排序，也可以指定排序的比较器，当用 Iterator遍历 TreeMap时，得到的记录是排过序的。 
如果使用排序的映射，建议使用 TreeMap。 在使用 TreeMap 时，key 必须实现 Comparable 接口或者在构造 TreeMap 传入自定义的
Comparator，否则会在运行时抛出 java.lang.ClassCastException类型的异常。 
* 参考：https://www.ibm.com/developerworks/cn/java/j-lo-tree/index.html 

### LinkHashMap（记录插入顺序） 

LinkedHashMap 是 HashMap 的一个子类，保存了记录的插入顺序，在用 Iterator 遍历LinkedHashMap时，先得到的记录肯定是先插入的，也可以在构造时带参数，按照访问次序排序。 
* 参考 1：http://www.importnew.com/28263.html 
* 参考 2：http://www.importnew.com/20386.html#comment-648123 

### HashMap,Hashtable,HashSet的区别
