### Java并发知识库

![](../_media/01/img_53_33.jpg)

### JAVA线程实现/创建方式 
#### 继承 Thread类 

Thread 类本质上是实现了 Runnable 接口的一个实例，代表一个线程的实例。启动线程的唯一方法就是通过 Thread 类的 start()实例方法。start()方法是一个 native方法，它将启动一个新线
程，并执行 run()方法。 
```java
public class MyThread extends Thread {   
  public void run() {   
   System.out.println("MyThread.run()");   
  }   
}   
MyThread myThread1 = new MyThread();   
myThread1.start();  
```

#### 实现 Runnable接口

如果自己的类已经 extends另一个类，就无法直接 extends Thread，此时，可以实现一个Runnable接口。 
```java
public class MyThread extends OtherClass implements Runnable {   
  public void run() {   
   System.out.println("MyThread.run()");   
  }   
}    
//启动MyThread，需要首先实例化一个 Thread，并传入自己的MyThread实例： 
MyThread myThread = new MyThread();   
Thread thread = new Thread(myThread);   
thread.start();   
//事实上，当传入一个 Runnable target参数给 Thread后，Thread的 run()方法就会调用
target.run() 
public void run() {   
  if (target != null) {   
   target.run();   
  }   
}  
```

#### ExecutorService、Callable<Class>、Future有返回值线程 

有返回值的任务必须实现 Callable接口，类似的，无返回值的任务必须 Runnable接口。执行
Callable任务后，可以获取一个 Future的对象，在该对象上调用 get就可以获取到 Callable任务
返回的 Object了，再结合线程池接口 ExecutorService就可以实现传说中有返回结果的多线程
了。 
```java
	//创建一个线程池  
	ExecutorService pool = Executors.newFixedThreadPool(taskSize);  
	// 创建多个有返回值的任务  
	List<Future> list = new ArrayList<Future>();  
	for (int i = 0; i < taskSize; i++) {  
		Callable c = new MyCallable(i + " ");
		// 执行任务并获取 Future对象
		Future f = pool.submit(c);
		list.add(f);
	}
	// 关闭线程池  
	pool.shutdown();  
	//获取所有并发任务的运行结果  
	for (Future f : list) {
		//从 Future对象上获取任务的返回值，并输出到控制台
		System.out.println("res：" + f.get().toString());  
	}
```

#### 基于线程池的方式 

线程和数据库连接这些资源都是非常宝贵的资源。那么每次需要的时候创建，不需要的时候销毁，是非常浪费资源的。那么我们就可以使用缓存的策略，也就是使用线程池。
 ```java
// 创建线程池 
ExecutorService threadPool = Executors.newFixedThreadPool(10); 
while(true) { 
	threadPool.execute(new Runnable() { // 提交多个线程任务，并执行 
		@Override 
		public void run() { 
			System.out.println(Thread.currentThread().getName() + " is running .."); 
			try { 
				Thread.sleep(3000); 
			} catch (InterruptedException e) { 
				e.printStackTrace(); 
			} 
		} 
	}); 
}
```
### 4种线程池

Java 里面线程池的顶级接口是 Executor，但是严格意义上讲 Executor 并不是一个线程池，而只是一个执行线程的工具。真正的线程池接口是 ExecutorService。 

![](../_media/01/img_56_34.png)

#### newCachedThreadPool

创建一个可根据需要创建新线程的线程池，但是在以前构造的线程可用时将重用它们。对于执行很多短期异步任务的程序而言，这些线程池通常可提高程序性能。调用 execute 将重用以前构造
的线程（如果线程可用）。如果现有线程没有可用的，则创建一个新线程并添加到池中。终止并从缓存中移除那些已有 60 秒钟未被使用的线程。因此，长时间保持空闲的线程池不会使用任何资
源。

#### newFixedThreadPool

创建一个可重用固定线程数的线程池，以共享的无界队列方式来运行这些线程。在任意点，在大多数 nThreads 线程会处于处理任务的活动状态。如果在所有线程处于活动状态时提交附加任务，
则在有可用线程之前，附加任务将在队列中等待。如果在关闭前的执行期间由于失败而导致任何线程终止，那么一个新线程将代替它执行后续的任务（如果需要）。在某个线程被显式地关闭之
前，池中的线程将一直存在。 

#### newScheduledThreadPool

创建一个线程池，它可安排在给定延迟后运行命令或者定期地执行。
```java
ScheduledExecutorService scheduledThreadPool= Executors.newScheduledThreadPool(3);
scheduledThreadPool.schedule(newRunnable(){
	@Override
	public void run() {
		System.out.println("延迟三秒");
	}
}, 3, TimeUnit.SECONDS); 
scheduledThreadPool.scheduleAtFixedRate(newRunnable(){
	@Override
	public void run() { 
		System.out.println("延迟 1秒后每三秒执行一次");
	}
},1,3,TimeUnit.SECONDS); 
```
#### newSingleThreadExecutor

Executors.newSingleThreadExecutor()返回一个线程池（这个线程池只有一个线程）,这个线程池可以在线程死后（或发生异常时）重新启动一个线程来替代原来的线程继续执行下去！

### 线程生命周期(状态)

#### 新建状态（NEW）

当程序使用 new关键字创建了一个线程之后，该线程就处于新建状态，此时仅由 JVM为其分配内存，并初始化其成员变量的值 

#### 就绪状态（RUNNABLE）

当线程对象调用了 start()方法之后，该线程处于就绪状态。Java虚拟机会为其创建方法调用栈和程序计数器，等待调度运行。 

#### 运行状态（RUNNING）

如果处于就绪状态的线程获得了 CPU，开始执行 run()方法的线程执行体，则该线程处于运行状态。 

#### 阻塞状态（BLOCKED）

阻塞状态是指线程因为某种原因放弃了 cpu 使用权，也即让出了 cpu timeslice，暂时停止运行。直到线程进入可运行(runnable)状态，才有机会再次获得 cpu timeslice 转到运行(running)状
态。阻塞的情况分三种：  
1. 等待阻塞（o.wait->等待对列）： 运行(running)的线程执行 o.wait()方法，JVM会把该线程放入等待队列(waitting queue)中。 
2. 同步阻塞(lock->锁池) 运行(running)的线程在获取对象的同步锁时，若该同步锁被别的线程占用，则 JVM会把该线程放入锁池(lock pool)中。 
3. 其他阻塞(sleep/join) 运行(running)的线程执行 Thread.sleep(long ms)或 t.join()方法，或者发出了 I/O请求时，JVM会把该线程置为阻塞状态。当 sleep()状态超时、join()等待线程终止或者超时、或者 I/O处理完毕时，线程重新转入可运行(runnable)状态。 

#### 线程死亡（DEAD） 
线程会以下面三种方式结束，结束后就是死亡状态。
1. 正常结束 run()或 call()方法执行完成，线程正常结束。 
2. 异常结束 线程抛出一个未捕获的 Exception或 Error。 
3. 调用 stop 直接调用该线程的 stop()方法来结束该线程—该方法通常容易导致死锁，不推荐使用。 

![](../_media/01/img_59_35.png)

### 终止线程 4种方式 

#### 正常运行结束 

程序运行结束，线程自动结束。

#### 使用退出标志退出线程

一般 run()方法执行完，线程就会正常结束，然而，常常有些线程是伺服线程。它们需要长时间的运行，只有在外部某些条件满足的情况下，才能关闭这些线程。使用一个变量来控制循环，例如：
最直接的方法就是设一个boolean类型的标志，并通过设置这个标志为 true 或 false 来控制 while 循环是否退出，代码示例：
 ```java
public class ThreadSafe extends Thread { 
    public volatile boolean exit = false;  
        public void run() {  
        while (!exit){ 
            //do something 
        } 
    }  
} 
```
定义了一个退出标志 exit，当 exit为 true时，while循环退出，exit的默认值为 false 在定义 exit 时，使用了一个 Java 关键字 volatile，这个关键字的目的是使 exit 同步，也就是说在同一时刻只
能由一个线程来修改 exit的值。

#### Interrupt方法结束线程

使用 interrupt()方法来中断线程有两种情况： 

1. 线程处于阻塞状态：
如使用了 sleep,同步锁的 wait,socket 中的 receiver,accept 等方法时，会使线程处于阻塞状态。当调用线程的 interrupt()方法时，会抛出 InterruptException 异常。
阻塞中的那个方法抛出这个异常，通过代码捕获该异常，然后 break 跳出循环状态，从而让我们有机会结束这个线程的执行。通常很多人认为只要调用 interrupt 方法线程就会结束，实
际上是错的， 一定要先捕获 InterruptedException异常之后通过 break来跳出循环，才能正常结束 run方法。 
2. 线程未处于阻塞状态：
使用 isInterrupted()判断线程的中断标志来退出循环。当使用interrupt()方法时，中断标志就会置 true，和使用自定义的标志来控制循环是一样的道理。  

```java
public class ThreadSafe extends Thread { 
    public void run() {  
        while (!isInterrupted()){ //非阻塞过程中通过判断中断标志来退出 
            try{ 
                Thread.sleep(5*1000);//阻塞过程捕获中断异常来退出 
            }catch(InterruptedException e){ 
                e.printStackTrace(); 
                break;//捕获到异常之后，执行 break跳出循环 
            } 
        } 
    }  
} 
```

#### stop方法终止线程（线程不安全）

程序中可以直接使用 thread.stop()来强行终止线程，但是 stop方法是很危险的，就象突然关闭计算机电源，而不是按正常程序关机一样，可能会产生不可预料的结果，不安全主要是：
thread.stop()调用之后，创建子线程的线程就会抛出 ThreadDeatherror 的错误，并且会释放子线程所持有的所有锁。一般任何进行加锁的代码块，都是为了保护数据的一致性，如果在调用
thread.stop()后导致了该线程所持有的所有锁的突然释放(不可控制)，那么被保护数据就有可能呈现不一致性，其他线程在使用这些被破坏的数据时，有可能导致一些很奇怪的应用程序错误。
`因此，并不推荐使用 stop方法来终止线程。`

### sleep与 wait 区别

1. 对于 sleep()方法，我们首先要知道该方法是属于 Thread类中的。而 wait()方法，则是属于Object类中的。  
2. sleep()方法导致了程序暂停执行指定的时间，让出 cpu该其他线程，但是他的监控状态依然保持者，当指定的时间到了又会自动恢复运行状态。 
3. 在调用 sleep()方法的过程中，线程不会释放对象锁。 
4. 而当调用 wait()方法的时候，线程会放弃对象锁，进入等待此对象的等待锁定池，只有针对此对象调用 notify()方法后本线程才进入对象锁定池准备获取对象锁进入运行状态。

### start与 run区别

1. start（）方法来启动线程，真正实现了多线程运行。这时无需等待 run 方法体代码执行完毕，可以直接继续执行下面的代码。 
2. 通过调用 Thread 类的 start()方法来启动一个线程， 这时此线程是处于就绪状态， 并没有运行。 
3. 方法 run()称为线程体，它包含了要执行的这个线程的内容，线程就进入了运行状态，开始运行 run函数当中的代码。 Run方法运行结束， 此线程终止。然后 CPU再调度其它线程。 

### Java后台线程

1. 定义：守护线程--也称“服务线程”，他是后台线程，它有一个特性，即为用户线程提供公共服务，在没有用户线程可服务时会自动离开。 
2. 优先级：守护线程的优先级比较低，用于为系统中的其它对象和线程提供服务。 
3. 设置：通过 setDaemon(true)来设置线程为“守护线程”；将一个用户线程设置为守护线程的方式是在 线程对象创建 之前 用线程对象的 setDaemon方法。 
4. 在 Daemon线程中产生的新线程也是 Daemon的。 
5. 线程则是 JVM级别的，以 Tomcat 为例，如果你在 Web 应用中启动一个线程，这个线程的生命周期并不会和 Web 应用程序保持同步。也就是说，即使你停止了 Web 应用，这个线程依旧是活跃的。 
6. example: 垃圾回收线程就是一个经典的守护线程，当我们的程序中不再有任何运行的Thread,程序就不会再产生垃圾，垃圾回收器也就无事可做，所以当垃圾回收线程是 JVM 上仅剩的线程时，垃圾回收线程会自动离开。它始终在低级别的状态中运行，用于实时监控和管理系统中的可回收资源。 
7. 生命周期：守护进程（Daemon）是运行在后台的一种特殊进程。它独立于控制终端并且周期性地执行某种任务或等待处理某些发生的事件。也就是说守护线程不依赖于终端，但是依赖于系统，与系统“同生共死”。当 JVM中所有的线程都是守护线程的时候，JVM就可以退出了；如果还有一个或以上的非守护线程则 JVM不会退出。

### Java锁
#### 乐观锁
#### 悲观锁 
#### 自旋锁 
#### Synchronized同步锁 
#### ReentrantLock 
#### Semaphore 信号量
#### AtomicInteger
#### 可重入锁（递归锁）
#### 公平锁与非公平锁
#### ReadWriteLock 读写锁
#### 共享锁和独占锁
#### 重量级锁（Mutex Lock）
#### 轻量级锁
#### 偏向锁
#### 分段锁
#### 锁优化
### 线程基本方法 

线程相关的基本方法有 wait，notify，notifyAll，sleep，join，yield 等。

![](../_media/01/img_72_37.jpg)

#### 线程等待（wait） 

调用该方法的线程进入WAITING状态，只有等待另外线程的通知或被中断才会返回，需要注意的是调用 wait()方法后，会释放对象的锁。因此，wait方法一般用在同步方法或同步代码块中。 
#### 线程睡眠（sleep） 

sleep导致当前线程休眠，与 wait方法不同的是 sleep不会释放当前占有的锁,sleep(long)会导致线程进入 TIMED-WATING状态，而 wait()方法会导致当前线程进入WATING状态 
#### 线程让步（yield） 

yield 会使当前线程让出 CPU 执行时间片，与其他线程一起重新竞争 CPU 时间片。一般情况下，优先级高的线程有更大的可能性成功竞争得到 CPU 时间片，但这又不是绝对的，有的操作系统对
线程优先级并不敏感。 

#### 线程中断（interrupt） 

中断一个线程，其本意是给这个线程一个通知信号，会影响这个线程内部的一个中断标识位。这个线程本身并不会因此而改变状态(如阻塞，终止等)。 
1. 调用 interrupt()方法并不会中断一个正在运行的线程。也就是说处于 Running 状态的线程并不会因为被中断而被终止，仅仅改变了内部维护的中断标识位而已。 
2. 若调用 sleep()而使线程处于 TIMED-WATING状态，这时调用 interrupt()方法，会抛出InterruptedException,从而使线程提前结束 TIMED-WATING状态。 
3. 许多声明抛出 InterruptedException的方法(如 Thread.sleep(long mills方法))，抛出异常前，都会清除中断标识位，所以抛出异常后，调用 isInterrupted()方法将会返回 false。 
4. 中断状态是线程固有的一个标识位，可以通过此标识位安全的终止线程。比如,你想终止一个线程 thread的时候，可以调用 thread.interrupt()方法，在线程的 run方法内部可以根据 thread.isInterrupted()的值来优雅的终止线程。

#### Join等待其他线程终止 

join() 方法，等待其他线程终止，在当前线程中调用一个线程的 join() 方法，则当前线程转为阻塞
状态，回到另一个线程结束，当前线程再由阻塞状态变为就绪状态，等待 cpu 的宠幸。
 
#### 为什么要用 join()方法？ 

很多情况下，主线程生成并启动了子线程，需要用到子线程返回的结果，也就是需要主线程需要
在子线程结束后再结束，这时候就要用到 join() 方法。
 ```java
System.out.println(Thread.currentThread().getName() + "线程运行开始!"); 
Thread6 thread1 = new Thread6();  
thread1.setName("线程 B"); 
thread1.join(); 
System.out.println("这时 thread1执行完毕之后才能执行主线程"); 
```

#### 线程唤醒（notify） 

Object 类中的 notify() 方法，唤醒在此对象监视器上等待的单个线程，如果所有线程都在此对象上等待，则会选择唤醒其中一个线程，选择是任意的，并在对实现做出决定时发生，线程通过调
用其中一个 wait() 方法，在对象的监视器上等待，直到当前的线程放弃此对象上的锁定，才能继续执行被唤醒的线程，被唤醒的线程将以常规方式与在该对象上主动同步的其他所有线程进行竞
争。类似的方法还有 notifyAll() ，唤醒再次监视器上等待的所有线程。 

#### 其他方法： 
1. sleep()：强迫一个线程睡眠Ｎ毫秒。  
2. isAlive()： 判断一个线程是否存活。  
3. join()： 等待线程终止。  
4. activeCount()： 程序中活跃的线程数。  
5. enumerate()： 枚举程序中的线程。  
6. currentThread()： 得到当前线程。  
7. isDaemon()： 一个线程是否为守护线程。  
8. setDaemon()： 设置一个线程为守护线程。(用户线程和守护线程的区别在于，是否等待主线程依赖于主线程结束而结束)  
9. setName()： 为线程设置一个名称。  
10. wait()： 强迫一个线程等待。
11. notify()： 通知一个线程继续运行。
12. setPriority()：设置一个线程的优先级。
13. getPriority(): 获得一个线程的优先级。

### 线程上下文切换 

巧妙地利用了时间片轮转的方式, CPU 给每个任务都服务一定的时间，然后把当前任务的状态保存下来，在加载下一任务的状态后，继续服务下一任务，任务的状态保存及再加载, 这段过程就叫做
上下文切换。时间片轮转的方式使多个任务在同一颗 CPU 上执行变成了可能。

![](../_media/01/img_74_38.png)

### 同步锁与死锁
### 线程池原理
### JAVA阻塞队列原理