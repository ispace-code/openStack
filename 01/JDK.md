
### JDK 
   JDK(Java development kit)是 Java 语言的软件开发工具包，主要用于移动设备、嵌入式设备上的java应用程序。JDK是整个java开发的核心，它包含了JAVA的运行环境（JVM+Java系统类库）和JAVA工具。
  #### JDK 主要版本变化
  ##### JDK5
```
foreach 迭代方式
可变参数
枚举
自动拆装箱
泛型
注解等重要特性。
```
  ##### JDK6
```
Desktop 类和 SystemTray 类，
使用 Complier API,
轻量级 HTTPServer API,
对脚步语言得支持,
Common Annotations 等重要特性
```
  ##### JDK7
```
Switch 支持字符串作为匹配条件
泛型类型自动推断
try-with-resources 资源关闭技巧
Object 工具类
ForkJoinPool 等重要类与特性。
```
  ##### JDK8
```
Lambda 表达式
函数式接口
方法引用、构造器引用和数组引用
接口支持默认方法和静态方法
Stream API
增强类型推断
新的日期时间 API
Optional 类
重复注解和类型注解
```
  ##### JDK9
```
Jigsaw 模块化项目
简化进程 API
轻量级 JSON API
钱和货币 API
进程改善和锁机制优化
代码分段缓存等重要特性。
```
  ##### JDK10
```
局部变量得自动推断，
改进 GC 和内存管理
线程本地握手
备用内存设备上的堆分配等重要特性
```
  ##### JDK11
```
删除了 Java EE 和 CORBA 模块
增加了嵌套的访问控制
支持动态类文件常量
改进 Aarch64 内联函数
提供实验性质得可扩展的的延迟垃圾收集器 ZGC 等重要特性
```
  ##### JDK12
```
对 Switch 进行了增强，除了使用 statement 还可以使用 expression, 325：Switch Expressions(Preview)
```
  ##### JDK13
```
扩展应用程序类-数据共享 JPE 350
增强 ZGC 以将未使用的堆内存返回给操作系统 JPE 351
使用易于维护和调试的更简单、更现代的实现替换 java.net.Socket 和 java.net.ServerSocket API 使用的底层实现 JPE 353
可在生产环境中使用的 switch 表达式，JDK 13 中将带来一个 beta 版本实现 JPE 354
将文本块添加到 Java 语言。JPE 355
文本块是一个多行字符串文字，它避免了对大多数转义序列的需要，以可预测的方式自动格式化字符串，并在需要时让开发人员控制格式。
```
### JRE
   JRE(Java runtime environment) Java运行环境（Java Runtime Environment，简称JRE）是一个软件，JVM标准实现及Java核心类库，JRE可以让计算机系统运行Java应用程序（Java Application）。

#### JRE 系统库
| 包名 | 说明 |
|--|--|
| resources.jar        | 提示信息显示国际化的包，里面各地区的文字,图片等。|
| rt.jar               | 运行时包,java核心源代码包。|
| jsse.jar             | SSL连接，验证的包。|
| jce.jar              | Java 加密扩展类库，含有很多非对称加密算法在里面，但也是可扩展的。|
| charsets.jar         | Java 字符集，这个类库中包含 Java 所有支持字符集的字符。|
| access-bridge-64.jar | 供对实现Java Accessibility API的GUI工具包的访问。|
| cldrdata.jar         | CLDR - Unicode Common Locale Data Repository Unicode CLDR为软件提供了支持世界语言的关键构建块，提供了最大和最广泛的语言环境数据库。|
| dnsns.jar            | 即DNS naming service ,提供DNS地址服务。|
| jaccess.jar          | 定义Assistive Technologies.AWT（Abstract Window Toolkit）使用的JDK实用程序类。|
| localedata.jar       | 日期显示国际化的包，里面包含各地区的日期文字。|
| nashorn.jar          | 1.动态链接.包含用于链接调用的动态调用站点的接口和类。  2.JVM的JavaScript解析引擎|
| sunec.jar            | JCE providers for Java Cryptography APIs|
| sunjce_provider.jar  | 为JCE 提供的加密安全套件|
| sunmscapi.jar        |  |
| sunpkcs11.jar        | PKCS#11 证书工具。|
| zipfs.jar            | java 对zip文件操作的支持。|
| tools.jar            | 是工具类库,编译和运行需要的|
