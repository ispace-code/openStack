### 类加载机制

#### 什么是类加载？
类的加载指的是将类的.class文件中的二进制数据读入到内存（JVM）中，将其放在运行时数据放入方法区内（这里方法区也称永久代，但是在Jdk1.8后取消这块改名叫元空间），然后在堆内（heap）创建一个java.lang.Class对象，用来封装类在方法区内的数据结构。类的加载的最终产品是位于堆区中的Class对象，Class对象封装了类在方法区内的数据结构，并且向Java程序员提供了访问方法区内的数据结构的接口。

#### 类的生命周期

一个类的生命周期包括：加载，验证，准备，解析，初始化的五个过程，这个五个过程。其中加载的过程对于开发人员来说是可控制的，至于原因就在加载的过程中包括三个阶段：通过一个类的全限定类名来获取该类的二进制字节流，将二进制字节流所表示的静态存储结构转到方法区所运行时的数据结构，最后在堆中生成代表该类的java.lang.Class的对象，作为方法区数据访问的入口，而由于JVM并没有规定而我们如果获取该类的二进制字节流，所以我们可以使用默认的类加载和自定义的类加载。

#### 类加载的层次图

在上面我们提到了开发人员可以使用默认的类加载或是自定义类加载器，这样我们就会想到如果保证这些类加载不会产生冲突呢？首先我们需要先了解一下JDK默认的几种类加载器：
(1) Bootstrap ClassLoader:
启动类加载器（Bootstrap ClassLoader）：由C++语言实现（针对HotSpot）,负责将存放在JAVA_HOME下jre\lib目录或-Xbootclasspath参数指定的路径中的虚拟机识别的类库加载到内存中（譬如 rt.jar）。
(2) Extension ClassLoader :
负责加载jre\lib\ext目录或java.ext.dirs系统变量指定的路径中的所有类库。
(3) Application ClassLoader :
是由 Sun 的 AppClassLoader（sun.misc.Launcher$AppClassLoader）实现的。它负责将系统类路径（CLASSPATH）中指定的类库加载到内存中。开发者可以直接使用系统类加载器。由于这个类加载器是ClassLoader中的getSystemClassLoader()方法的返回值，因此一般称为SystemClassLoader
以上就是jdk默认的三种类加载器，下图我们可以看到JDK的类加载层次，图中的箭头就是一种双亲委派的加载模式。什么是双亲委派模型呢？
双亲委派模型：某一个特定的类加载器接受一个类加载的请求时候，首先先把这个类的请求委托给上级的类加载器，即父类的类加载器完成，而不是自己优先尝试加载该类，只有当父类无法加载该类的时候，自己再尝试加载该类，如果自己也无法加载即抛出：ClassNotFoundException 和 NoClassDefFoundError。
至于为什么要使用这种类加载模型呢？举一个网上都用的例子：类java.lang.Object，它存在在rt.jar中，无论哪一个类加载器要加载这个类，最终都是委派给处于模型最顶端的Bootstrap ClassLoader进行加载，因此Object类在程序的各种类加载器环境中都是同一个类。相反，如果没有双亲委派模型而是由各个类加载器自行加载的话，即便是同一个.class文件，由不同的类加载器加载即不是同一个类，如果用户编写了一个java.lang.Object的同名类并放在ClassPath中，那系统中将会出现多个不同的Object类，程序将混乱。因此，如果开发者尝试编写一个与rt.jar类库中重名的Java类，可以正常编译，但是永远无法被加载运行。


我们可以看到jdk里面的classloader方法的实现，首先会尝试看本地有没有加载过该类，如果加载过，即直接返回，否则获取该类加载器的父类，如果父类不等于空，则优先委托给父类来加载，如果父类为空，则直接交给顶级的类加载器完成加载，如果顶级类加载也无法加载，则才调用自己的类加载来findClass来加载该类。


#### Tomcat类加载

现在终于走到正题，想必一定是tomcat并没有完全遵循双亲委派的累加机制，否则不会单独拿出来讲。首先我们先思考几个问题：
1.如果在一个Tomcat内部署多个应用，甚至多个应用内使用了某个类似的几个不同版本，但它们之间却互不影响。这是如何做到的。
2.如果多个应用都用到了某类似的相同版本，是否可以统一提供，不在各个应用内分别提供，占用内存呢。
至于第一个问题其实上面的讲解已经解答了，就是因为tomcat部署了多个应用，而多个应用都采用自定义的类加载器，所以即便是同一个类使用不同的类加载机制最终也是不一样的类。
至于第二问题，我首先看看Tomcat的类加载层次：
我们看到webappClassLoader上面有一个common的类加载器，它是所有webappClassLoader的父加载器，多个应用汇存在公有的类库，而公有的类库都会使用commonclassloader来实现。这样也就回答了第二个问题；
由此我们也引出了如果不是公有的类呢，这些类就会使用webappClassLoader加载，而webappClassLoader的实现并没有走双亲委派的模式，这有是为何呢？
原因有两个：
1）加载本类的classloader未知时，为了隔离不同的调用者，即类的隔离，采用了上下文类加载的模式加载类；
2）当前高层的接口在低层去实现，而高层的类有需要低层的类加载的时候，这个时候，需要使用上下文类加载器去实现（后面会通过JDBC的加载来讲解）
JDCB的类加载（经典的线程上下文加载器）
private static Connection getConnection(
String url,java.util.Properties info,Class caller)throwsSQLException {
//由于DriverManger.class是由于jdk里的rt.jar包里面加载的，而实际调用的是com.mysql.jdbc.Driver的driver该类，而是调用getConnection的方法时候，下图中的这个方法的到DriverManger这个类是顶级类加载器加载的，这个时候又要启动该类的子类，所以双亲委派是无法加载该类的，即图二中，caller.getClassLoader是null，这个时候就会调用 if 里面的线程上下文的加载器，通过上下文加载的方式完成加载，最好验证该是否可用，完成获取JDBC的连接。

有点跑题，现在回到Tomcat类加载中，我们需要了解到底是采用了双亲委派还是上线文加载模式。首先我们需要明确的一点就是基础类肯，common类，还是有servlet-api一定用双亲委派模式，因为这些都是公有的类库，且对于Servlet-api是不允许被重写，也就是说如果你用自己的类加载的话，会影响到应用内部得到正常运行了，也就是说只有加载app应用的类时候才会引用上下文加载。下面我们看看上线文加载的类：webappLoader；
在Tomcat启动时，会创建一系列的类加载器，在其主类Bootstrap的初始化过程中，会先初始化classloader，然后将其绑定到Thread中。
其中initClassLoaders方法，会根据catalina.properties的配置，创建相应的classloader。由于默认只配置了common.loader属性，所以其中只会创建一个出来commonClassLoader，然后，当一个应用启动的时候，会为其创建对应的WebappClassLoader。此时会将commonClassLoader设置为其parent。下面的代码是StandardContext类在启动时创建WebappLoader的代码
这里的getParentClassLoader会从当前组件的classLoader一直向上，找parent classLoader设置。之后注意下一行代码
webappLoader.setDelegate
这就是在设置后面Web应用的类查找时是父优先还是子优先。这个配置可以在server.xml里，对Context组件进行配置。
即在Context元素下可以嵌套一个Loader元素，配置Loader的delegate即可，其默认为false，即子优先。类似于这样
delegate="true"/>
注意Loader还有一个属性是reloadable，用于表明对于/WEB-INF/classes/ 和 /WEB-INF/lib 下资源发生变化时，是否重新加载应用。这个特性在开发的时候，还是很有用的。
如果你的应用并没有配置这个属性，想要重新加载一个应用，只需要使用manager里的reload功能就可以。
有点跑题，回到我们说的delgate上面来，配置之后，可以指定Web应用类加载时，到底是使用父优先还是子优先。
这里的WebappLoader，就开始了正式的创建WebappClassLoader，而在WebbappClassLoader里具体逻辑如下：判断已加载的类里是否已经包含，然后避免Java SE的classes被覆盖，packageAccess的检查。之后，开始了我们的父优先子优先的流程。这里判断是否使用delegate时，对于一些容器提供的class，也会跳过。
boolean delegateLoad = delegate ||filter(name);
而由于上面提到通常delegateLoad这个字段是false，所以普通我们的Tomcat在web应用类加载的时候，都会走上线文加载。最后补充一点，也是在网上查阅资料看我们常用的Class.forName()这个方法，其实我们往往忽略了该方法还有两个参数，一个是是否必须初始化，另指定加载该类的类加载器，也就是说，在forName方法中，我也是可以指定获取该类的类加载器的呀


![](../_media/01/classLoader-1.png)
![](../_media/01/classLoader-2.png)
![](../_media/01/classLoader-3.png)
![](../_media/01/classLoader-4.png)